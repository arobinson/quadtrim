#/**
# * Copyright (c) 2012-2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
# * All rights reserved.
# *
# * Redistribution and use in source and binary forms, with or without
# * modification, are permitted provided that the following conditions are met:
# *     * Redistributions of source code must retain the above copyright
# *       notice, this list of conditions and the following disclaimer.
# *     * Redistributions in binary form must reproduce the above copyright
# *       notice, this list of conditions and the following disclaimer in the
# *       documentation and/or other materials provided with the distribution.
# *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
# *       names of its contributors may be used to endorse or promote products
# *       derived from this software without specific prior written permission.
# *
# * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# */
#
# Builds QuAdTrim
#
# Compile:
# - make
#   This will place the executable in the bin/ directory which you can install where you like
#
# Use libargtable2 instead of getopt
# - make ARGTABLE=Y
#
# Add support for Gzip files
# - make GZIP=Y
# - make GZIP=OLD   # (for libz 1.2.5 and below)
#
# Author: Andrew Robinson
#

VERSION=2.0.3
CC=gcc
PREFIX=/usr/local

# conditional includes/libs
CLIBS_ARGTABLE_Y=-largtable2
CLIBS_GZIP_Y=-lz
CLIBS_GZIP_OLD=-lz
CFLAGS_ARGTABLE_Y=-DUSE_ARGTABLE
CFLAGS_GZIP_Y=-DUSE_ZLIB
CFLAGS_GZIP_OLD=-DUSE_ZLIB_OLD

CLIBS=$(CLIBS_ARGTABLE_$(ARGTABLE)) $(CLIBS_GZIP_$(GZIP)) -std=gnu90
CFLAGS=$(CFLAGS_ARGTABLE_$(ARGTABLE)) $(CFLAGS_GZIP_$(GZIP)) -DVERSION="\"$(VERSION)\"" -std=gnu90

# filename modifier
GZ_Y=-gzip
GZ_OLD=-gzipold
ARG_Y=argtab
OPTNAME=$(GZ_$(GZIP))$(ARG_$(ARGTABLE))
#TODO: modify the executable name too and provide wrapper script
CPLNAME_gcc=gcc
CPLNAME_g++=gcc
CPLNAME_icc=intel
CPLNAME=$(CPLNAME_$(CC))


# master includes/libs/options
INCLUDE=
OPTS=-O3 -Wall

# targets
all: prep quadtrim

check: test

test:
	python test_src/TestQuAdTrim.py

fulltest:
	make clean
	echo "[[No Libs]]"
	make all
	make test
	echo "[[GZip Lib]]"
	make GZIP=Y all
	make test
	echo "[[GZip (old) Lib]]"
	make GZIP=OLD all
	make test
	echo "[[Argtable2 Lib]]"
	make ARGTABLE=Y all
	make test
	echo "[[Gzip & Argtable2 Libs]]"
	make GZIP=Y ARGTABLE=Y all
	make test
	echo "[[Gzip (old) & Argtable2 Libs]]"
	make GZIP=OLD ARGTABLE=Y all
	make test

prep:
	mkdir -p build bin

build/quadtrim$(OPTNAME).o: src/quadtrim.c src/quadtrim.h
	$(CC) $(OPTS) $(CFLAGS) -o build/quadtrim$(OPTNAME).o -c src/quadtrim.c $(INCLUDE)

quadtrim: build/quadtrim$(OPTNAME).o
	$(CC) $(OPTS) -o bin/quadtrim  ./build/quadtrim$(OPTNAME).o  $(CLIBS)

install: quadtrim
	mkdir -p $(PREFIX)/bin $(PREFIX)/man/man1
	cp bin/quadtrim $(PREFIX)/quadtrim/$(VERSION)-$(CPLNAME)/bin
	cp man/quadtrim.1 $(PREFIX)/quadtrim/$(VERSION)-$(CPLNAME)/man/man1

installalt: quadtrim
	mkdir -p $(PREFIX)/quadtrim/$(VERSION)-$(CPLNAME)/bin $(PREFIX)/quadtrim/$(VERSION)-$(CPLNAME)/man/man1
	cp bin/quadtrim $(PREFIX)/quadtrim/$(VERSION)-$(CPLNAME)/bin
	cp man/quadtrim.1 $(PREFIX)/quadtrim/$(VERSION)-$(CPLNAME)/man/man1
	

clean:
	rm -f build/* bin/*
	rm -rf `ls -1 | grep "^test_" | grep -v "test_src"`

## Unit testing applications ##
unittest: unittestbuild
	python test_src/unittestrunner.py IsAdapter
	python test_src/unittestrunner.py AdapterTree
	python test_src/unittestrunner.py QualityCheckWinAvg

unittestbuild: bin/testIsAdapter bin/testAdapterTree bin/testQualityCheckWinAvg

bin/test%: build/test%$(OPTNAME).o build/quadtrim$(OPTNAME)nm.o
	$(CC) $(OPTS) -o $@ $^ $(CLIBS)

build/quadtrim$(OPTNAME)nm.o: src/quadtrim.c src/quadtrim.h
	$(CC) $(OPTS) $(CFLAGS) -o $@ -c $< $(INCLUDE) -DNOMAIN

.PRECIOUS: build/test%$(OPTNAME).o
build/test%$(OPTNAME).o: test_src/test%.c test_src/common.c test_src/common.h
	$(CC) $(OPTS) $(CFLAGS) -o $@ -c $< $(INCLUDE)



# EOF #
