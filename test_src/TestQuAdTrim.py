'''
/**
 * Copyright (c) 2012-2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
'''

import unittest
import os, shutil, subprocess, sys, time


## Settings ##
# display stdout when running test
DEBUG = False
#DEBUG = True

# time each test (and output on stdout)
DEBUGTIME = False


## Testing code ##
# a dictionary to compliment bases
comp = {'A': 'T','T': 'A','C': 'G','G': 'C','N': '',}


_sequence = "TGACTTACGACAATGCCCATGATGTTGAGCTCATCGGGCGATGTCGATTCAGGCGCGGTTCATAGTATTCGAGCATTGGCAAAACC"\
            "TCCTTTGCAATCCTCTGATCTGTCAGCAACAGAAAGAAACAGGAGGTTATCAGATGAGGATAAGGGATATCTCGCTTGCAAAGGAA"

_adaptor =  "ACAGGCTGCGGAAATTACGTTAGTCCCGTCAGTAAAATTACAGATAGGCGATCGTGATGTGGCTATTACTGGGATGGAGGTCACTG"\
            "CGCGACCACGGCTGGTGGAAACAACATTGAATGGCGAGGCAATCGCTGGCACCTACACGGACCGCCGCCACCGCCGCGCCACCATA"

def reverseComp(seq):
    '''make the reverse complement'''
    _seqcomp = [comp[b] for b in seq]
    _seqcomp.reverse()
    return "".join(_seqcomp)

def reverse(seq):
    '''make the reverse of a string'''
    _seqcomp = [comp[b] for b in seq]
    _seqcomp.reverse()
    return "".join(_seqcomp)
    

testInFileName1 = 'test1.fq'
testInFileName2 = 'test2.fq'
testInFileName1GZ = '%s.gz' % testInFileName1
testInFileName2GZ = '%s.gz' % testInFileName2

testOutFileName1 = 'test1-pass.fq'
testOutFileName2 = 'test2-pass.fq'
testOutFileName1GZ = '%s.gz' % testOutFileName1
testOutFileName2GZ = '%s.gz' % testOutFileName2

testDiscardFileName1 = 'test1-discard.fq'
testDiscardFileName2 = 'testX-discard.fq'
testDiscardFileName1GZ = '%s.gz' % testOutFileName1
testDiscardFileName2GZ = '%s.gz' % testOutFileName2

testSingletonFileName = 'testX-singleton.fq'

testAdapterFileName1 = 'testX-adapter.fq'
testAdapterFileName1GZ = '%s.gz' % testOutFileName1

quAdTrim = os.path.dirname(os.path.abspath(__file__)) + '/../bin/quadtrim'


'''
Char   !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
       |              |          |    |        |                              |                     |
Ascii 33             44         59   64       73                            104                   126
Phred  0.2...........15.........26...31........41 
'''


class TestQuAdTrim(unittest.TestCase):
    '''A super class for test classes that performs testing on QuAdTrim'''
    
    currentResult = None
    errorcount = 0
    
    def setUp(self):
        if DEBUG:
            print("\n\nRunning: %s" % self._testMethodName)
        if os.path.exists(self._testMethodName):
            shutil.rmtree(self._testMethodName)
        os.mkdir(self._testMethodName)
        os.chdir(self._testMethodName)
        
        # store error count for later (hack)
        self.errorcount = len(self.currentResult.errors) + len(self.currentResult.failures)
        self.startTime = time.time()
        
        self.stdout = ""
        
    def run(self, result=None):
        self.currentResult = result # remember result for use in tearDown
        unittest.TestCase.run(self, result) # call superclass run method
        
    def tearDown(self):
        os.chdir('..')
        
        # hack to cleanup only when successful
        errors = len(self.currentResult.errors) + len(self.currentResult.failures)
        if errors == self.errorcount and not DEBUG:
            shutil.rmtree(self._testMethodName)
        elif errors != self.errorcount:
            print self.stdout
        self.errorcount = errors
        
        if DEBUGTIME:
            print "\n%s: %.3f seconds" % (self._testMethodName, time.time() - self.startTime)

    def assertFileLen(self, filename, length, msg):
        f = open(filename, 'r')
        self.assertEqual(len(f.readlines()), length, msg)

    def assertSeqEquals(self, filename, seq, msg):
        handle = open(filename, "rU")
        handle.readline()
        fseq = handle.readline().rstrip()
        self.assertEqual(fseq, seq, "%s, No sequence in file (%s, \n%s, \n%s)" % (msg, filename, fseq, seq))
        handle.close()
    
    def assertFileNotExists(self, filename, msg):
        filefound = os.path.exists(filename)
        self.assertFalse(filefound, msg)
    
    def assertFileExists(self, filename, msg):
        filefound = os.path.exists(filename)
        self.assertTrue(filefound, msg)
        
    def assertFileContentEquals(self, filename, lines, msg):
        '''Checks if the file contains only the lines provided'''
        handle = open(filename, "rU")
        flines = striplist(handle.readlines())
        self.assertEquals(flines, lines, msg)
        
    def assertFileSeqEquals(self, filename, lines, msg):
        '''Checks if the file contains only the sequences provided (in fastq format)'''
        handle = open(filename, "rU")
        flines = striplist(handle.readlines()[1::4])
        self.assertEquals(flines, lines, msg)
        
    def assertStdOutLinesStartWith(self, lines, count, msg):
        '''Checks that stdout contains 'count' lines starting with one of 'lines' '''
        c = 0
        matches = []
        for line in self.stdout.splitlines(False):
            for start in lines:
                if line.startswith(start):
                    c += 1
                    matches.append(line)
                    break
        if c != count:
            for match in matches:
                print "Match: %s" % match 
        self.assertEqual(c, count, msg)
        
    #Support functions that can run various commands
    def runQuAdTrim(self, mode=6, nbase=None, start=None, outzip=None, checkzip=None, dumpAdapter=None, taboutput=None, outdir=None, idir=None, \
                    trimMode=None, adapters=None, adaptFilterRate=None, gtail=False):
        '''Runs the program'''
        infile2 = None
        
        filename1GZ = testInFileName1GZ
        filename2GZ = testInFileName2GZ
        filename1 = testInFileName1
        filename2 = testInFileName2
        filename1dat = testInFileName1.replace(".fq", ".dat")
        filename2dat = testInFileName2.replace(".fq", ".dat")
        if idir is not None:
            filename1GZ = "%s/%s" %(idir,testInFileName1GZ)
            filename2GZ = "%s/%s" %(idir,testInFileName2GZ)
            filename1 = "%s/%s" %(idir,testInFileName1)
            filename2 = "%s/%s" %(idir,testInFileName2)
            filename1dat = "%s/%s" %(idir,testInFileName1.replace(".fq", ".dat"))
            filename2dat = "%s/%s" %(idir,testInFileName2.replace(".fq", ".dat"))
            
        if os.path.exists(filename1GZ):
            infile1 = filename1GZ
        elif os.path.exists(filename1dat):
            infile1 = filename1dat
        else:
            infile1 = filename1
        if os.path.exists(filename2GZ):
            infile2 = filename2GZ
        elif os.path.exists(filename2dat):
            infile2 = filename2dat
        elif os.path.exists(filename2):
            infile2 = filename2
        
        args = [quAdTrim]
        if dumpAdapter is not None:
            args.append('-D')
            args.append(dumpAdapter)
        if adaptFilterRate is not None:
            args.append('-F')
            args.append(adaptFilterRate)
        if gtail:
            args.append("-g")
        if mode is not None:
            args.append('-m')
            args.append(mode)
        if outdir is not None:
            args.append('-O')
            args.append(outdir)
        if taboutput is not None:
            args.append('-t')
        if nbase is not None:
            args.append('-N')
            args.append(nbase)
        if start == True:
            args.append('-s')
        if adapters is not None:
            for adapter in adapters:
                args.append('-S')
                args.append(adapter)
        if trimMode is not None:
            args.append('-T')
            args.append(trimMode)
        if outzip is not None:
            args.append('-z')
            args.append(outzip)
        if checkzip == True:
            args.append('-Z')
        args.append(infile1)
        if infile2:
            args.append(infile2)
        args = map(str, args)
#         print " ".join(args)
        return self.runcmd(args)

    def writeSeq1(self, seqs, gz=False, odir=None):
        '''Writes sequences to a file'''
        filename = testInFileName1
        if odir is not None:
            filename = "%s/%s" %(odir,testInFileName1)
        outFile = open(filename, 'w')
        for seq in seqs:
            outFile.write('@')
            outFile.write(seq[0])
            outFile.write('\n')
            outFile.write(seq[1])
            outFile.write('\n+\n')
            outFile.write(seq[2])
            outFile.write('\n')
        outFile.close()
        if gz:
            self.runcmd(['gzip', filename])
            
    def writeSeq2(self, seqs, gz=False, odir=None):
        '''Writes sequences to a file'''
        filename = testInFileName2
        if odir is not None:
            filename = "%s/%s" %(odir,testInFileName2)
        outFile = open(filename, 'w')
        for seq in seqs:
            outFile.write('@')
            outFile.write(seq[0])
            outFile.write('\n')
            outFile.write(seq[1])
            outFile.write('\n+\n')
            outFile.write(seq[2])
            outFile.write('\n')
        outFile.close()
        if gz:
            self.runcmd(['gzip', filename])

    def runcmd(self, cmd):
        '''Runs a command on the commandline.  Expects a list containing the cmd and any arguments'''
        fnull = open(os.devnull, 'w')
        if fnull:
            if DEBUG:
                print "CMD: %s" %(cmd,)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            self.stdout = p.communicate()[0]
            fnull.close()
            return p.returncode
        else:
            raise Exception("Unable to hide output of run")
        return 222
            
def striplist(l):
    '''Strips each element of list'''
    def s(s):
        return str(s).strip()
    return map(s,l)

## test classes ##

class TestAdapter(TestQuAdTrim):
    '''Adapter trimming mode'''
    
    def test_49_insert(self):
        insert = _sequence[:49]
        adaptor = _adaptor[:51]
        self.writeSeq1([('test', insert + adaptor, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adaptor, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertFileLen(testDiscardFileName2, 8, "Insert size of 49 should be rejected (length)")
    
    def test_50_insert(self):
        insert = _sequence[:50]
        adaptor = _adaptor[:50]
        self.writeSeq1([('test', insert + adaptor, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adaptor, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert), "Read 2 is not trimmed properly")
    
    def test_51_insert(self):
        insert = _sequence[:51]
        adaptor = _adaptor[:49]
        self.writeSeq1([('test', insert + adaptor, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adaptor, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert), "Read 2 is not trimmed properly")
    
    def test_70_insert(self):
        insert = _sequence[:70]
        adaptor = _adaptor[:30]
        self.writeSeq1([('test', insert + adaptor, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adaptor, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert), "Read 2 is not trimmed properly")
        
    def test_99_insert(self):
        insert = _sequence[:99]
        adaptor = _adaptor[:1]
        self.writeSeq1([('test', insert + adaptor, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adaptor, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert), "Read 2 is not trimmed properly")

    def test_100_insert(self):
        insert = _sequence[:100]
        self.writeSeq1([('test', insert, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert), qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert), "Read 2 is not trimmed properly")

    def test_101_insert(self):
        insert = _sequence[:101]
        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")

    def test_150_insert(self):
        insert = _sequence[:150]
        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")

    def test_150_insert_unequal(self):
        '''Checks adapter ignores paired sequences with unequal read lengths'''
        insert = _sequence[:150]
        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert)[:99], qual((20,)) * 99)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not skipped")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:99], "Read 2 is not skipped")

    def test_199_insert(self):
        insert = _sequence[:199]
        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")

    def test_200_insert(self):
        insert = _sequence[:200]
        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")

    def test_dump0_15(self):
        '''dump off option works'''
        insert = _sequence[:85]
        adapter = _adaptor[:15]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + reverseComp(adapter), qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, dumpAdapter=0)
        self.assertFileNotExists(testAdapterFileName1, "Adapter file shouldn't exist with adapter dumping off")
        
    def test_dump15_30(self):
        '''dumps adapters of 30 with -D 15'''
        insert = _sequence[:70]
        adapter = _adaptor[:30]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + reverseComp(adapter), qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, dumpAdapter=15)
        self.assertFileLen(testAdapterFileName1, 8, "Adapter file should contain 2 sequences with dumping on")
        self.assertFileSeqEquals(testAdapterFileName1, [adapter, reverseComp(adapter)], "Adapter file doesn't contain correct adapter pieces")
        
    def test_dump15_16(self):
        '''dumps adapters of 16 with -D 15'''
        insert = _sequence[:84]
        adapter = _adaptor[:16]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + reverseComp(adapter), qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, dumpAdapter=15)
        self.assertFileExists(testAdapterFileName1, "Adapter file should exist with adapter dumping on")
        self.assertFileLen(testAdapterFileName1, 8, "Adapter file should contain 2 sequences with dumping on")
        self.assertFileSeqEquals(testAdapterFileName1, [adapter, reverseComp(adapter)], "Adapter file doesn't contain correct adapter pieces")
        
    def test_dump15_15(self):
        '''dumps adapters of 15 with -D 15'''
        insert = _sequence[:85]
        adapter = _adaptor[:15]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + reverseComp(adapter), qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, dumpAdapter=15)
        self.assertFileExists(testAdapterFileName1, "Adapter file should exist with adapter dumping on")
        self.assertFileLen(testAdapterFileName1, 8, "Adapter file should contain 2 sequences with dumping on")
        self.assertFileSeqEquals(testAdapterFileName1, [adapter, reverseComp(adapter)], "Adapter file doesn't contain correct adapter pieces")
        
    def test_dump15_14(self):
        '''doesn't dump adapters of 14 with -D 15'''
        insert = _sequence[:86]
        adapter = _adaptor[:14]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + reverseComp(adapter), qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, dumpAdapter=15)
        self.assertFileLen(testAdapterFileName1, 0, "Adapter file should contain 0 sequences with short adapter")
        
    def test_dump1_1(self):
        '''dumps adapters of 1 with -D 1'''
        insert = _sequence[:99]
        adapter = _adaptor[:1]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + reverseComp(adapter), qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, dumpAdapter=1)
        self.assertFileLen(testAdapterFileName1, 8, "Adapter file should contain 2 sequences with dumping on")
        self.assertFileSeqEquals(testAdapterFileName1, [adapter, reverseComp(adapter)], "Adapter file doesn't contain correct adapter pieces")
        
    def test_adapter_filtering_ok(self):
        '''filters adapters with a correct adapter'''
        insert = _sequence[:70]
        adapter = _adaptor[:30]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adapters=[adapter])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert], "Read 1 not trimmed properly")
        
    def test_adapter_filtering_noise(self):
        '''filters adapters with an incorrect adapter'''
        insert = _sequence[:70]
        adapter = _adaptor[:30]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adapters=["ACCACGGCTGGTGGAAACAACATTGAATGGCGAGGCAATC"])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert + adapter], "Read 1 should not be trimmed")
        
    def test_adapter_filtering_1in10error(self):
        '''filters adapters with a correct adapter with 1 in 10 bases are wrong'''
        insert = _sequence[:70]
        usedadapter  = "ACAGGCTGCGGAAATTACGTTAGTCCCGTC"
        realadapter =  "ACAGGCTGCGCAAATTACGTAAGTCCCGTG"
        self.writeSeq1([('test', insert + usedadapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + usedadapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adapters=[realadapter])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert], "Read 1 not trimmed properly")
        
    def test_adapter_filtering_1in9p67error(self):
        '''filters adapters with a correct adapter with 1 in 9.67 bases are wrong'''
        insert = _sequence[:70]
        usedadapter  = "ACAGGCTGCGGAAATTACGTTAGTCCGTC"
        realadapter =  "ACAGGCTGCGCAAATTACGTAAGTCCGTG"
        self.writeSeq1([('test', insert + usedadapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + usedadapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adapters=[realadapter])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert + usedadapter], "Read 1 should not be trimmed")
        
    def test_adapter_filtering_1in10error_start(self):
        '''filters adapters with a correct adapter with 1 in 10 bases are wrong (at start)'''
        insert = _sequence[:70]
        usedadapter  = "ACAGGCTGCGGAAATTACGTTAGTCCCGTC"
        realadapter =  "GGGGGCTGCGGAAATTACGTTAGTCCCGTC"
        self.writeSeq1([('test', insert + usedadapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + usedadapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adapters=[realadapter])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert], "Read 1 not trimmed properly")
        
    def test_adapter_filtering_1in5error_f0p2(self):
        '''filters adapters with a correct adapter with 1 in 5 bases are wrong'''
        insert = _sequence[:70]
        usedadapter  = "ACAGGCTGCGGAAATTACGTTAGTCCCGTC"
        realadapter =  "ACAGGGTGCGCAAATAACGTAAGTCGCGTG"
        self.writeSeq1([('test', insert + usedadapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + usedadapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adaptFilterRate=0.2, adapters=[realadapter])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert], "Read 1 not trimmed properly")
    
    def test_adapter_2x_filtering_1st(self):
        '''filters 2x adapters with a correct adapter (first)'''
        insert = _sequence[:70]
        adapter = _adaptor[:30]
        adapter2 = _adaptor[60:90]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adapters=[adapter, adapter2])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert], "Read 1 not trimmed properly")
        
    def test_adapter_2x_filtering_2nd(self):
        '''filters 2x adapters with a correct adapter (second)'''
        insert = _sequence[:70]
        adapter = _adaptor[:30]
        adapter2 = _adaptor[60:90]
        self.writeSeq1([('test', insert + adapter, qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert) + adapter, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=0, adapters=[adapter2, adapter])
        self.assertFileLen(testOutFileName1, 4, "Read 1 is not trimmed properly, No sequence in file")
        self.assertFileSeqEquals(testOutFileName1, [insert], "Read 1 not trimmed properly")
        
#     def test_adapter_discovery1a(self):
#         '''Tests auto-discovery of sequences works'''
#         self.runcmd(['ln', '-s', '../read1-10000.fq', 'test1.fq'])
#         self.runcmd(['ln', '-s', '../read2-10000.fq', 'test2.fq'])
#         rc = self.runQuAdTrim(mode=1, trimMode=1, dumpAdapter=1)
#         self.assertEqual(rc, 0, "Return code should be 0 (actual: %s)" % rc)
#         self.assertFileLen(testOutFileName1, 9964*4, "Incorrect number of read1 sequences passed")
#         self.assertFileLen(testOutFileName2, 9964*4, "Incorrect number of read2 sequences passed")
#         
#     def test_adapter_discovery1b(self):
#         '''Tests auto-discovery of sequences works'''
#         self.runcmd(['ln', '-s', '../read1-10000.fq', 'test1.fq'])
#         self.runcmd(['ln', '-s', '../read2-10000.fq', 'test2.fq'])
#         rc = self.runQuAdTrim(mode=1, trimMode=2, dumpAdapter=1)
#         self.assertEqual(rc, 0, "Return code should be 0 (actual: %s)" % rc)
#         self.assertFileLen(testOutFileName1, 9964*4, "Incorrect number of read1 sequences passed")
#         self.assertFileLen(testOutFileName2, 9964*4, "Incorrect number of read2 sequences passed")
#         
#     def test_adapter_discovery2(self):
#         '''Tests auto-discovery of sequences works'''
#         self.runcmd(['ln', '-s', '../read1-100000.fq', 'test1.fq'])
#         self.runcmd(['ln', '-s', '../read2-100000.fq', 'test2.fq'])
#         rc = self.runQuAdTrim(mode=1, trimMode=2, dumpAdapter=1)
#         self.assertEqual(rc, 0, "Return code should be 0 (actual: %s)" % rc)
#         self.assertFileLen(testOutFileName1, 99635*4, "Incorrect number of read1 sequences passed")
#         self.assertFileLen(testOutFileName2, 99635*4, "Incorrect number of read2 sequences passed")
        
    def test_adapter_discovery_len39(self):
        '''Checks discovery of adapter with 2x 39 base adapter'''
        insert = _sequence[:61]
        adapter1 = _adaptor[:38] + "T"
        adapter2 = _adaptor[:38] + "A"
        self.writeSeq1([('test1', insert + adapter1, qual((20,)) * 100),('test2', insert + adapter2, qual((20,)) * 100)])
        self.writeSeq2([('test1', reverseComp(insert) + adapter1, qual((20,)) * 100),('test2', reverseComp(insert) + adapter2, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=1)
        
        self.assertStdOutLinesStartWith(["Adapter: %s" % adapter1, "Adapter: %s" % adapter2], 2, "Expected only 2 adapter seqs")
#        self.assertEqual(True, False, "Stopper")
        
    def test_adapter_discovery_len40(self):
        '''Checks discovery of adapter with 2x 40 base adapter'''
        insert = _sequence[:60]
        adapter1 = _adaptor[:39] + "T"
        adapter2 = _adaptor[:39] + "A"
        self.writeSeq1([('test1', insert + adapter1, qual((20,)) * 100),('test2', insert + adapter2, qual((20,)) * 100)])
        self.writeSeq2([('test1', reverseComp(insert) + adapter1, qual((20,)) * 100),('test2', reverseComp(insert) + adapter2, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=1)
        
        self.assertStdOutLinesStartWith(["Adapter: %s" % adapter1, "Adapter: %s" % adapter2], 2, "Expected only 2 adapter seqs")
#        self.assertEqual(True, False, "Stopper")
        
    def test_adapter_discovery_len41(self):
        '''Checks discovery of adapter with 2x 41 base adapter'''
        insert = _sequence[:59]
        adapter1 = _adaptor[:40] + "T"
        adapter2 = _adaptor[:40] + "A"
        self.writeSeq1([('test1', insert + adapter1, qual((20,)) * 100),('test2', insert + adapter2, qual((20,)) * 100)])
        self.writeSeq2([('test1', reverseComp(insert) + adapter1, qual((20,)) * 100),('test2', reverseComp(insert) + adapter2, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=1)
        
        self.assertStdOutLinesStartWith(["Adapter: %s" % adapter1[:40]], 1, "Expected only 1 adapter seq")
        
    def test_adapter_discovery_len42(self):
        '''Checks discovery of adapter with 2x 42 base adapter'''
        insert = _sequence[:58]
        adapter1 = _adaptor[:41] + "T"
        adapter2 = _adaptor[:41] + "A"
        self.writeSeq1([('test1', insert + adapter1, qual((20,)) * 100),('test2', insert + adapter2, qual((20,)) * 100)])
        self.writeSeq2([('test1', reverseComp(insert) + adapter1, qual((20,)) * 100),('test2', reverseComp(insert) + adapter2, qual((20,)) * 100)])
        self.runQuAdTrim(mode=1, trimMode=1)
        
        self.assertStdOutLinesStartWith(["Adapter: %s" % adapter1[:40]], 1, "Expected only 1 adapter seq")
        
        
    
#    def test_largefile(self):
#        self.runcmd(['ln', '-s', '../large1.fq', 'test1.fq'])
#        self.runcmd(['ln', '-s', '../large2.fq', 'test2.fq'])
#        rc = self.runQuAdTrim(mode=1, dumpAdapter=10)
#        self.assertEquals(rc, 0, "Large file should have return code 0")
#
#    def test_xlargefile(self):
#        self.runcmd(['ln', '-s', '../xlarge1.fq', 'test1.fq'])
#        self.runcmd(['ln', '-s', '../xlarge2.fq', 'test2.fq'])
#        rc = self.runQuAdTrim(mode=1, dumpAdapter=10)
#        self.assertEquals(rc, 0, "Large file should have return code 0")
#
#    def test_xlargefile1(self):
#        self.runcmd(['ln', '-s', '../xlarge2.1.fq', 'test1.fq'])
#        self.runcmd(['ln', '-s', '../xlarge2.2.fq', 'test2.fq'])
#        rc = self.runQuAdTrim(mode=1) #, dumpAdapter=10)
#        self.assertEquals(rc, 0, "Large file should have return code 0")
#
#    def test_xlargefile2(self):
#        self.runcmd(['ln', '-s', '../xlarge2.1.fq', 'test1.fq'])
#        self.runcmd(['ln', '-s', '../xlarge2.2.fq', 'test2.fq'])
#        rc = self.runQuAdTrim(mode=2) #, dumpAdapter=10)
#        self.assertEquals(rc, 0, "Large file should have return code 0")
#
#    def test_xlargefile3(self):
#        self.runcmd(['ln', '-s', '../xlarge2.1.fq', 'test1.fq'])
#        self.runcmd(['ln', '-s', '../xlarge2.2.fq', 'test2.fq'])
#        rc = self.runQuAdTrim(mode=3) #, dumpAdapter=10)
#        self.assertEquals(rc, 0, "Large file should have return code 0")


   

class TestRepeatedSequences(TestQuAdTrim):
    '''Tests adapter trimming of repeated sequences'''

    def test_2b_repeat(self):
        '''Tests with a very large 2base repeated sequence'''
        insert = "TG" * 100
        readlen = 100
        self.writeSeq1([('test', insert[:readlen], qual((20,)) * readlen)])
        self.writeSeq2([('test', reverseComp(insert)[:readlen], qual((20,)) * readlen)])
        self.runQuAdTrim(mode=1, dumpAdapter=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:readlen], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:readlen], "Read 2 is not trimmed properly")

    def test_2b_repeat_odd(self):
        '''Tests with a very large 2base repeated sequence with odd length'''
        insert = ("TG" * 100)[1:]
        readlen = 100
        self.writeSeq1([('test', insert[:readlen], qual((20,)) * readlen)])
        self.writeSeq2([('test', reverseComp(insert)[:readlen], qual((20,)) * readlen)])
        self.runQuAdTrim(mode=1, dumpAdapter=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:readlen], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:readlen], "Read 2 is not trimmed properly")

    def test_5b_repeat(self):
        '''Tests with a very large 5base repeated sequence'''
        insert = _sequence[:5] * 40
        readlen = 100
        self.writeSeq1([('test', insert[:readlen], qual((20,)) * readlen)])
        self.writeSeq2([('test', reverseComp(insert)[:readlen], qual((20,)) * readlen)])
        self.runQuAdTrim(mode=1, dumpAdapter=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:readlen], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:readlen], "Read 2 is not trimmed properly")

    def test_40b_repeat(self):
        '''Tests with a very long 40base repeated sequence'''
        insert = _sequence[:40] * 5
        readlen = 100
        self.writeSeq1([('test', insert[:readlen], qual((20,)) * readlen)])
        self.writeSeq2([('test', reverseComp(insert)[:readlen], qual((20,)) * readlen)])
        self.runQuAdTrim(mode=1, dumpAdapter=1, trimMode=0)
        self.assertSeqEquals(testOutFileName1, insert[:readlen], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:readlen], "Read 2 is not trimmed properly")

#    def test_40b_repeat_subs(self):
#        '''Tests with a very long 40base repeated sequence with substitutions'''
#        insert = _sequence[:40] * 5
#        readlen = 100
#        self.writeSeq1([('test', insert[:readlen], qual((20,)) * readlen)])
#        self.writeSeq2([('test', reverseComp(insert)[:readlen], qual((20,)) * readlen)])
#        self.runQuAdTrim(mode=1, dumpAdapter=1)
#        self.assertSeqEquals(testOutFileName1, insert[:readlen], "Read 1 is not trimmed properly")
#        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:readlen], "Read 2 is not trimmed properly")

#    def test_2b_repeat_with_subs(self):
#        '''Tests with a very large 2base repeated sequence with some substitutions'''
#        insert = "TG" * 100
#        self.runQuAdTrim(mode=1)
#        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)])
#        self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)])
#        self.runQuAdTrim(mode=1)
#        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
#        self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")


class TestAvgQual(TestQuAdTrim):
    '''Checks the avg quality functions correctly'''
    
    def test_low_avg_quality14(self):
        self.writeSeq1([('test', _sequence[:60], qual((14,)) * 60)])
        self.runQuAdTrim(mode=2)
        self.assertFileLen(testDiscardFileName1, 4, "Avg Quality of 14 should fail")
    
#    def test_low_avg_quality19p99(self):
#        self.writeSeq1([('test', _sequence[:100], (qual((20,)) * 99) + qual((19,)))])
#        self.runQuAdTrim(mode=2)
#        self.assertFileLen(testDiscardFileName1, 4, "Avg Quality of 19.99 should fail")
    
    def test_low_avg_quality20(self):
        self.writeSeq1([('test', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=2)
        self.assertFileLen(testOutFileName1, 4, "Avg Quality of 20 should pass")
        

class TestEndTrimming(TestQuAdTrim):
    '''Checks that the end of a sequence is trimmed correctly'''
    
    def test_end_trim4(self):
        seq = _sequence[:70]
        qualph = [21]*60
        qualph.extend(range(20,10,-1))
        qualstr = qual(qualph)
        self.writeSeq1([('test', seq, qualstr)])
        self.runQuAdTrim(mode=2)
        self.assertSeqEquals(testOutFileName1, _sequence[:66], "End trim should be at base 66 (...%s)" % (_sequence[60:66],))
    
    def test_end_trim1(self):
        seq = _sequence[:70]
        qualph = [21]*69
        qualph.append(13)
        qualstr = qual(qualph)
        self.writeSeq1([('test', seq, qualstr)])
        self.runQuAdTrim(mode=2)
        self.assertSeqEquals(testOutFileName1, _sequence[:69], "End trim should be at base 69 (...%s)" % (_sequence[63:69],))
    
    def test_end_trim0(self):
        seq = _sequence[:70]
        qualph = [21]*70
        qualstr = qual(qualph)
        self.writeSeq1([('test', seq, qualstr)])
        self.runQuAdTrim(mode=2)
        self.assertSeqEquals(testOutFileName1, _sequence[:70], "No trimming should be required")

class TestChastityFilter(TestQuAdTrim):
    ''''''
    
    def test_chastity_Y(self):
        self.writeSeq1([('M1:59:TEST:6:1101:2266:2233 1:Y:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=4)
        self.assertFileLen(testDiscardFileName1, 4, "Chastity on should filter sequences with chastity fail flag")
    
    def test_chastity_N(self):
        self.writeSeq1([('M1:59:TEST:6:1101:2266:2233 1:N:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=4)
        self.assertFileLen(testOutFileName1, 4, "Chastity on should not filter Sequences with chastity pass flag")
        
    def test_nochastity_Y(self):
        self.writeSeq1([('M1:59:TEST:6:1101:2266:2233 1:Y:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=8)
        self.assertFileLen(testOutFileName1, 4, "Chastity off should not filter Sequences with chastity fail flag")
    
    def test_nochastity_N(self):
        self.writeSeq1([('M1:59:TEST:6:1101:2266:2233 1:N:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=8)
        self.assertFileLen(testOutFileName1, 4, "Chastity off should not filter Sequences with chastity pass flag")
        
    def test_chastity_YN(self):
        self.writeSeq1([('M1:59:TEST:6:1101:2266:2233 1:Y:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.writeSeq2([('M1:59:TEST:6:1101:2266:2233 2:N:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=4)
        self.assertFileLen(testDiscardFileName2, 4, "Chastity on should filter sequences with chastity fail flag")
        self.assertFileLen(testSingletonFileName, 4, "Chastity on should filter sequences with chastity fail flag")
        
    def test_chastity_NY(self):
        self.writeSeq1([('M1:59:TEST:6:1101:2266:2233 1:N:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.writeSeq2([('M1:59:TEST:6:1101:2266:2233 2:Y:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=4)
        self.assertFileLen(testDiscardFileName2, 4, "Chastity on should filter sequences with chastity fail flag")
        self.assertFileLen(testSingletonFileName, 4, "Chastity on should filter sequences with chastity fail flag")
        
    #TODO: check that Chastity works with other modes as well

class TestNBaseCount(TestQuAdTrim):
    '''Checks that the N Base counter works'''
    
    def test_n_base_with_qualtrim(self):
        '''Checks the nbase check within quality trim rejects 3n's with -N 2'''
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[22] = 'N'
        seq[48] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.runQuAdTrim(mode=10, nbase=2)
        self.assertFileLen(testDiscardFileName1, 4, "3x N's should fail '-N 2' option")
    
    def test_n_base2_spread_qual_qualtrim(self):
        '''Checks the nbase check within quality trim accepts 2n's with -N 2'''
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[48] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.runQuAdTrim(mode=10, nbase=2)
        self.assertFileLen(testOutFileName1, 4, "2x N's should pass '-N 2' option")
    
    def test_n_base_with_qualtrim_paired(self):
        '''Checks the nbase check within paired quality trim rejects 3n's with -N 2'''
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[22] = 'N'
        seq[48] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.writeSeq2([('test', _sequence[:60], qua)])
        self.runQuAdTrim(mode=10, nbase=2)
        self.assertFileLen(testDiscardFileName2, 4, "3x N's should fail '-N 2' option")
        self.assertFileLen(testSingletonFileName, 4, "3x N's should fail '-N 2' option")
    
    def test_n_base2_spread_qual_qualtrim_paired(self):
        '''Checks the nbase check within paired quality trim accepts 2n's with -N 2'''
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[48] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.writeSeq2([('test', _sequence[:60], qua)])
        self.runQuAdTrim(mode=10, nbase=2)
        self.assertFileLen(testOutFileName1, 4, "2x N's should pass '-N 2' option")
        self.assertFileLen(testOutFileName2, 4, "2x N's should pass '-N 2' option")
        
    
    def test_n_base3_spread_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[22] = 'N'
        seq[48] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testDiscardFileName1, 4, "3x N's should fail '-N 2' option")
    
    def test_n_base2_spread_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[48] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testOutFileName1, 4, "2x N's should pass '-N 2' option")
    
    def test_n_base3_spread(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        self.writeSeq1([('test1', "".join(seq), "".join(qua))])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testDiscardFileName1, 4, "3x N's should fail '-N 2' option")
    
    def test_n_base2_spread(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        self.writeSeq1([('test', "".join(seq), "".join(qua))])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testOutFileName1, 4, "2x N's should pass '-N 2' option")
    
    def test_n_base3_grouped_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[21] = 'N'
        seq[22] = 'N'
        seq[23] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testDiscardFileName1, 4, "3x N's should fail '-N 2' option")
    
    def test_n_base2_grouped_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[22] = 'N'
        seq[23] = 'N'
        self.writeSeq1([('test', "".join(seq), qua)])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testOutFileName1, 4, "2x N's should pass '-N 2' option")
    
    def test_n_base3_grouped(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[21] = 'N'
        qua[21] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        seq[23] = 'N'
        qua[23] = '#'
        self.writeSeq1([('test', "".join(seq), "".join(qua))])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testDiscardFileName1, 4, "3x N's should fail '-N 2' option")
    
    def test_n_base2_grouped(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[21] = 'N'
        qua[21] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        self.writeSeq1([('test', "".join(seq), "".join(qua))])
        self.runQuAdTrim(mode=8, nbase=2)
        self.assertFileLen(testOutFileName1, 4, "2x N's should pass '-N 2' option")

class TestBothEndTrimming(TestQuAdTrim):
    '''Tests various scenarios with both end trimming'''
    
    def test_noncont_both(self):
        seq = _sequence[:70]
        qua = list(badbase*5 + goodbase*60 + badbase*5)
        qua[34] = badbase
        qua[35] = badbase
        self.writeSeq1([('test', seq, "".join(qua))])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 4, "2 bad bases and both bad ends should be accepted")
        self.assertSeqEquals(testOutFileName1, _sequence[5:65], "Trimming performed incorrectly")
    
    def test_2segs_short_long(self):
        seq = _sequence[:90]
        qua = badbase*5 + goodbase*20 + badbase*5 + goodbase*55 + badbase*5
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 4, "2 segments should be accepted")
        self.assertSeqEquals(testOutFileName1, _sequence[30:85], "Trimming performed incorrectly.  Only long segment should be kept")
    
    def test_2segs_long_short(self):
        seq = _sequence[:90]
        qua = badbase*5 + goodbase*55 + badbase*5 + goodbase*20 + badbase*5
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 4, "2 segments should be accepted")
        self.assertSeqEquals(testOutFileName1, _sequence[5:60], "Trimming performed incorrectly.  Only long segment should be kept")
    
    def test_2segs_long_long(self):
        seq = _sequence[:125]
        qua = badbase*5 + goodbase*55 + badbase*5 + goodbase*55 + badbase*5
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 4, "2 segments should be accepted")
        self.assertSeqEquals(testOutFileName1, _sequence[5:60], "Trimming performed incorrectly.  First long segment should be kept")
    
    def test_2segs_long_longer(self):
        seq = _sequence[:130]
        qua = badbase*5 + goodbase*55 + badbase*5 + goodbase*60 + badbase*5
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 4, "2 segments should be accepted")
        self.assertSeqEquals(testOutFileName1, _sequence[65:125], "Trimming performed incorrectly.  Only longer segment should be kept")
    
    def test_2segs_longer_long(self):
        seq = _sequence[:130]
        qua = badbase*5 + goodbase*60 + badbase*5 + goodbase*55 + badbase*5
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 4, "2 segments should be accepted")
        self.assertSeqEquals(testOutFileName1, _sequence[5:65], "Trimming performed incorrectly.  Only longer segment should be kept")
    
    def test_2segs_short_short(self):
        seq = _sequence[:130]
        qua = badbase*5 + goodbase*20 + badbase*5 + goodbase*25 + badbase*5
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testDiscardFileName1, 4, "2 segments should be rejected")
    
    def test_start_trim(self):
        seq = _sequence[:70]
        qua = qual(range(10,20)) + greatqual*60
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertSeqEquals(testOutFileName1, _sequence[5:70], "Start trim should be at base 5 (%s...)" % (_sequence[5:10],))
    
    def test_both_trim(self):
        seq = _sequence[:80]
        qua = qual(range(10,20)) + greatqual*60 + qual(range(20,10,-1))
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertSeqEquals(testOutFileName1, _sequence[5:76], "Both trim should be at base 5 to 76 (%s..%s)" % (_sequence[5:10],_sequence[71:76]))

    def test_low_avg_quality14_with_start(self):
        self.writeSeq1([('test', _sequence[:60], qual((14,)) * 60)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testDiscardFileName1, 4, "Avg Quality of 14 should fail")

    def test_low_avg_quality19p99_with_start(self):
        self.writeSeq1([('test', _sequence[:100], (qual((20,)) * 99) + qual((19,)))])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testDiscardFileName1, 4, "Avg Quality of 19.99 should fail")
    
    def test_low_avg_quality20_with_start(self):
        self.writeSeq1([('test', _sequence[:60], qual((20,)) * 60)])
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 4, "Avg Quality of 20 should pass")

class TestMultiFastq(TestQuAdTrim):
    '''Test with multiple sequences in fastq file'''

    def test_multi_fastq(self):
        seqs = []
        
        seq = list(_sequence[:60])
        qua = list(goodbase * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        seqs.append(('test1', "".join(seq), "".join(qua)))
        
        seq = list(_sequence[:60])
        qua = list(goodbase * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        seqs.append(('test2', "".join(seq), "".join(qua)))
        
        seqs.append(('test3', _sequence[:60], qual((14,)) * 60))
        
        self.writeSeq1(seqs)
        self.runQuAdTrim(mode=2, start=True)
        self.assertFileLen(testOutFileName1, 8, "Two seqs should pass")
        self.assertFileLen(testDiscardFileName1, 4, "One seq should fail")
        
class TestBufferOverflow(TestQuAdTrim):
    '''Checks to make sure it can handle a buffer overflow ok'''
    
    def test_name_overflow1024(self):
        nam = "test" + "a"*1017
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([(nam, seq, qua)])
        rc = self.runQuAdTrim()
        self.assertEqual(rc, 0, "Return code should be 0 with a 1024 char name")
        self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
    
    def test_name_overflow1025(self):
        nam = "test" + "a"*1018
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([(nam, seq, qua)])
        rc = self.runQuAdTrim(outzip=2)
        self.assertNotEqual(rc, 0, "Return code should be non-0 with a 1025 char name")
        #TODO: check for error message on STDERR
        
#    def test_seq_overflow2048(self):
#        nam = "test"
#        seq = _sequence[:102]*20
#        qua = goodbase*2048
#        self.writeSeq1([(nam, seq, qua)])
#        rc = self.runQuAdTrim()
#        self.assertEqual(rc, 0, "Return code should be 0 with a 2048 base seq")
#        self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
#        
#    def test_seq_overflow2049(self):
#        nam = "test"
#        seq = _sequence[:102]*20 + "A"
#        qua = goodbase*2049
#        self.writeSeq1([(nam, seq, qua)])
#        rc = self.runQuAdTrim()
#        self.assertNotEqual(rc, 0, "Return code should be non-0 with a 2049 base seq")
#        #TODO: check for error message on STDERR


class TestGZip(TestQuAdTrim):
    '''Tests the GZip input and output functions'''
    
    def test_gzip_to_plain(self):
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], gz=True)
        rc = self.runQuAdTrim(outzip=2)
        rcz = self.runQuAdTrim(checkzip=True)
        if rcz == 0:
            self.assertEqual(rc, 0, "Return code should be 0")
            self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 5, "Return code should be 5 when zip not supported")
    
    def test_plain_to_gzip(self):
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)])
        rc = self.runQuAdTrim(outzip=1)
        rc2 = self.runcmd(['gunzip', testOutFileName1 + ".gz"])
        rcz = self.runQuAdTrim(checkzip=True)
        if rcz == 0:
            self.assertEqual(rc, 0, "Return code should be 0")
            self.assertEqual(rc2, 0, "Gunzip failed to extract result file")
            self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 6, "Return code should be 6 when zip not supported (%s)" % rc)
    
    def test_gzip_to_gzip(self):
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], gz=True)
        rcz = self.runQuAdTrim(checkzip=True)
        rc = self.runQuAdTrim(outzip=1)
        if rcz == 0:
            self.runcmd(['gunzip', testInFileName1GZ])
            self.assertEqual(rc, 0, "Return code should be 0")
            rc = self.runcmd(['gunzip', testOutFileName1 + ".gz"])
            self.assertEqual(rc, 0, "Gunzip failed to extract result file")
            self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 5, "Return code should be 5 when zip not supported")
    
    def test_150_insert_gzip(self):
        
        rcz = self.runQuAdTrim(checkzip=True)
        if rcz == 0:
            insert = _sequence[:150]
            self.writeSeq1([('test', insert[:100], qual((20,)) * 100)], gz=True)
            self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)], gz=True)
            self.runQuAdTrim()
            rc = self.runcmd(['gunzip', testOutFileName1GZ])
            self.assertEqual(rc, 0, "Gunzip failed to extract read 1 file")
            rc = self.runcmd(['gunzip', testOutFileName2GZ])
            self.assertEqual(rc, 0, "Gunzip failed to extract read 2 file")
            self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
            self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")
    
    def test_150_insert_mixed_gzip1(self):
        '''Read1 compressed but Read2 not (output should be compressed)'''
        insert = _sequence[:150]
        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)], gz=True)
        self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)])
        rc = self.runQuAdTrim()
        rcz = self.runQuAdTrim(checkzip=True)
        if rcz == 0:
            rc2 = self.runcmd(['gunzip', testOutFileName1GZ])
            self.assertEqual(rc2, 0, "Gunzip failed to extract read 1 file")
            rc2 = self.runcmd(['gunzip', testOutFileName2GZ])
            self.assertEqual(rc2, 0, "Gunzip failed to extract read 2 file")
            self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
            self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")
        else:
            self.assertEqual(rc, 5, "Return code should be 5 when zip not supported (%s)" % rc)
#    
    def test_150_insert_mixed_gzip2(self):
        '''Read2 compressed but Read1 not (output should be plain)'''
        insert = _sequence[:150]
        self.writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        self.writeSeq2([('test', reverseComp(insert)[:100], qual((20,)) * 100)], gz=True)
        rc = self.runQuAdTrim()
        rcz = self.runQuAdTrim(checkzip=True)
        if rcz == 0:
            self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
            self.assertSeqEquals(testOutFileName2, reverseComp(insert)[:100], "Read 2 is not trimmed properly")
        else:
            self.assertEqual(rc, 5, "Return code should be 5 when zip not supported (%s)" % rc)
            

class TestFileExtension(TestQuAdTrim):
    '''Tests gzip and plain files with wrong extensions'''
    
    def test_gzip_ext_with_plain_content(self):
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)])
        self.runcmd(['ln', '-s', testInFileName1, testInFileName1GZ])
        rc = self.runQuAdTrim(mode=2)
        self.assertEqual(rc, 0, "Return code should be 0 (%s)" %rc)
        self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
        
    def test_plain_ext_with_gzip_content(self):
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], gz=True)
        self.runcmd(['mv', testInFileName1GZ, testInFileName1])
        rc = self.runQuAdTrim(mode=2)
        rcz = self.runQuAdTrim(checkzip=True)
        if rcz == 0: # support gzip
            self.assertEqual(rc, 0, "Return code should be 0 (%s)" %rc)
            rc = self.runcmd(['gunzip', testOutFileName1GZ])
            self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 5, "Return code should be 5 when zip not supported (%s)" %rc)
            
    def test_other_ext_with_gzip_content(self):
        '''tests .dat extension with gzip content'''
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], gz=True)
        self.runcmd(['mv', testInFileName1GZ, testInFileName1.replace(".fq",".dat")])
        rc = self.runQuAdTrim(mode=2)
        rcz = self.runQuAdTrim(checkzip=True)
        if rcz == 0: # support gzip
            self.assertEqual(rc, 0, "Return code should be 0 (%s)" %rc)
            rc = self.runcmd(['gunzip', testOutFileName1GZ])
            self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 5, "Return code should be 5 when zip not supported (%s)" %rc)
            
    def test_other_ext_with_plain_content(self):
        '''tests .dat extension with gzip content'''
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)])
        rc = self.runQuAdTrim(mode=2)
        self.assertEqual(rc, 0, "Return code should be 0 (%s)" %rc)
        self.assertFileLen(testOutFileName1, 4, "1 sequence should pass")
        
#    def test_

class TestOutdir(TestQuAdTrim):
    '''Test the outdir option works'''
    
    def test_out_cwd_default(self):
        '''Put output files in cwd (default)'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, idir="subdir")
        self.assertFileExists(testOutFileName1, "Output file missing (pass)")
        self.assertFileExists(testDiscardFileName1, "Output file missing (discard)")
        
    def test_out_cwd(self):
        '''Put output files in cwd'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, outdir=".", idir="subdir")
        self.assertFileExists(testOutFileName1, "Output file missing (pass)")
        self.assertFileExists(testDiscardFileName1, "Output file missing (discard)")
        
    def test_out_cwd_slash(self):
        '''Put output files in cwd'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, outdir="./", idir="subdir")
        self.assertFileExists(testOutFileName1, "Output file missing (pass)")
        self.assertFileExists(testDiscardFileName1, "Output file missing (discard)")
        
    def test_out_src(self):
        '''Put output files in src dir'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, outdir="<SRC>", idir="subdir")
        self.assertFileExists("subdir/%s"%testOutFileName1, "Output file missing (pass)")
        self.assertFileExists("subdir/%s"%testDiscardFileName1, "Output file missing (discard)")
        
    def test_out_other(self):
        '''Put output files in another dir'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=15, outdir="subdir")
        self.assertFileExists("subdir/%s"%testOutFileName1, "Output file missing (pass)")
        self.assertFileExists("subdir/%s"%testDiscardFileName1, "Output file missing (discard)")
    
    def test_out_cwd_default_paired(self):
        '''Put output files in cwd (default)'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.writeSeq2([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, idir="subdir")
        self.assertFileExists(testOutFileName1, "Output file missing (pass)")
        self.assertFileExists(testOutFileName2, "Output file missing (pass)")
        self.assertFileExists(testDiscardFileName2, "Output file missing (discard)")
        self.assertFileExists(testSingletonFileName, "Output file missing (singleton)")
        
    def test_out_cwd_paired(self):
        '''Put output files in cwd'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.writeSeq2([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, outdir=".", idir="subdir")
        self.assertFileExists(testOutFileName1, "Output file missing (pass)")
        self.assertFileExists(testOutFileName2, "Output file missing (pass)")
        self.assertFileExists(testDiscardFileName2, "Output file missing (discard)")
        self.assertFileExists(testSingletonFileName, "Output file missing (singleton)")
        
    def test_out_cwd_slash_paired(self):
        '''Put output files in cwd'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.writeSeq2([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, outdir="./", idir="subdir")
        self.assertFileExists(testOutFileName1, "Output file missing (pass)")
        self.assertFileExists(testOutFileName2, "Output file missing (pass)")
        self.assertFileExists(testDiscardFileName2, "Output file missing (discard)")
        self.assertFileExists(testSingletonFileName, "Output file missing (singleton)")
        
    def test_out_src_paired(self):
        '''Put output files in src dir'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)], odir="subdir")
        self.writeSeq2([('test', seq, qua)], odir="subdir")
        self.runQuAdTrim(mode=15, outdir="<SRC>", idir="subdir")
        self.assertFileExists("subdir/%s"%testOutFileName1, "Output file missing (pass)")
        self.assertFileExists("subdir/%s"%testOutFileName2, "Output file missing (pass)")
        self.assertFileExists("subdir/%s"%testDiscardFileName2, "Output file missing (discard)")
        self.assertFileExists("subdir/%s"%testSingletonFileName, "Output file missing (singleton)")
        
    def test_out_other_paired(self):
        '''Put output files in another dir'''
        os.mkdir("subdir")
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua)])
        self.writeSeq2([('test', seq, qua)])
        self.runQuAdTrim(mode=15, outdir="subdir")
        self.assertFileExists("subdir/%s"%testOutFileName1, "Output file missing (pass)")
        self.assertFileExists("subdir/%s"%testOutFileName2, "Output file missing (pass)")
        self.assertFileExists("subdir/%s"%testDiscardFileName2, "Output file missing (discard)")
        self.assertFileExists("subdir/%s"%testSingletonFileName, "Output file missing (singleton)")


class TestGTail(TestQuAdTrim):
    '''Checks the gtail trimmer functions correctly'''
    
    def test_good_gtail(self):
        self.writeSeq1([('test', _sequence[:60] + "G"*10, qual((20,))*70) ])
        rc = self.runQuAdTrim(mode=2, gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a good qual gtail")
        self.assertSeqEquals(testOutFileName1, _sequence[:60], "Gtail should be trimmed")
    
    def test_poor_gtail(self):
        self.writeSeq1([('test', _sequence[:60] + "G"*10, (qual((20,))*57) + (qual((14,)) * 13)) ])
        rc = self.runQuAdTrim(mode=2, gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a poor qual gtail")
        self.assertSeqEquals(testOutFileName1, _sequence[:57], "poor qual should be trimmed")
    
    def test_partpoor_gtail(self):
        self.writeSeq1([('test', _sequence[:60] + "G"*10, (qual((20,))*65) + (qual((14,)) * 5)) ])
        rc = self.runQuAdTrim(mode=2, gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a part poor tail")
        self.assertSeqEquals(testOutFileName1, _sequence[:60], "Gtail should be trimmed")
    
    def test_good_nogtail(self):
        self.writeSeq1([('test', _sequence[:60], qual((20,))*60) ])
        rc = self.runQuAdTrim(mode=2, gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with no gtail")
        self.assertSeqEquals(testOutFileName1, _sequence[:60], "No Gtail should be trimmed")
    
    def test_good_gtail_lowavgqaul(self):
        self.writeSeq1([('test', _sequence[:60] + "G"*10, (qual((19,))*60) + (qual((40,)) * 10)) ])
        rc = self.runQuAdTrim(mode=2, gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a low avg qual and high gtail")
        self.assertFileLen(testDiscardFileName1, 4, "1 sequence should fail for low avg qual")
        
        
class TestMisc(TestQuAdTrim):
    '''A few misc test cases'''
    
    def test_empty_sequence(self):
        self.writeSeq1([('test', '', '')])
        self.assertEqual(self.runQuAdTrim(mode=2, start=True), 0, 'None-zero return code for empty sequence')
        
    def test_plus_annotation(self):
        seq = _sequence[:60]
        qua = goodbase*60
        self.writeSeq1([('test', seq, qua, 'test')])
        self.assertEqual(self.runQuAdTrim(mode=2, start=True), 0, 'None-zero return code for annotation on "+" line')
        
    def test_all_mode(self):
        '''Run all trim and filtering modes with regular output'''
        seq = _sequence[:80]
        qua = qual(range(10,20)) + greatqual*60 + qual(range(20,10,-1))
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=15, start=True)
        self.assertSeqEquals(testOutFileName1, _sequence[5:76], "Both trim should be at base 5 to 76 (%s..%s)" % (_sequence[5:10],_sequence[71:76]))

    def test_all_mode_taboutput(self):
        '''Run all trim and filtering modes with tab output'''
        seq = _sequence[:80]
        qua = qual(range(10,20)) + greatqual*60 + qual(range(20,10,-1))
        self.writeSeq1([('test', seq, qua)])
        self.runQuAdTrim(mode=15, start=True, taboutput=True)
        self.assertSeqEquals(testOutFileName1, _sequence[5:76], "Both trim should be at base 5 to 76 (%s..%s)" % (_sequence[5:10],_sequence[71:76]))
        

## support functions ##

def revcomp(s):
    _comp = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}
    def comp(b):
        return _comp[b]
    return ''.join(reverseComp(map(comp,s)))



def qual(quals):
    '''returns a qualitystring of given (tuple/list) phred scores'''
    res = ""
    for sc in quals:
        res += str(unichr(sc+33))
    return res

greatqual = qual((38,))
goodbase = qual((21,))
avgbase = qual((16,))
badbase = qual((14,))



## test runner ##
if __name__ == '__main__':
    unittest.main()
