import unittest
import os, shutil, subprocess, sys

try:
    from Bio import SeqIO
except ImportError:
    print ("Missing BioPython")
    exit(1)

DEBUG = False

# a dictionary to compliment bases
comp = {
      'A': 'T',
      'T': 'A',
      'C': 'G',
      'G': 'C',
      'N': '',
      }


_sequence = "TGACTTACGACAATGCCCATGATGTTGAGCTCATCGGGCGATGTCGATTCAGGCGCGGTTCATAGTATTCGAGCATTGGCAAAACC"\
            "TCCTTTGCAATCCTCTGATCTGTCAGCAACAGAAAGAAACAGGAGGTTATCAGATGAGGATAAGGGATATCTCGCTTGCAAAGGAA"

_adaptor =  "ACAGGCTGCGGAAATTACGTTAGTCCCGTCAGTAAAATTACAGATAGGCGATCGTGATGTGGCTATTACTGGGATGGAGGTCACTG"\
            "CGCGACCACGGCTGGTGGAAACAACATTGAATGGCGAGGCAATCGCTGGCACCTACACGGACCGCCGCCACCGCCGCGCCACCATA"

def reverse(seq):
    '''make the reverse complement'''
    _seqcomp = [comp[b] for b in seq]
    _seqcomp.reverse()
    return "".join(_seqcomp)

testInFileName1 = 'test1.fq'
testInFileName2 = 'test2.fq'
testInFileName1GZ = '%s.gz' % testInFileName1
testInFileName2GZ = '%s.gz' % testInFileName2

testOutFileName1 = 'test1-fix.fq'
testOutFileName2 = 'test2-fix.fq'
testOutFileName1GZ = '%s.gz' % testOutFileName1
testOutFileName2GZ = '%s.gz' % testOutFileName2

pythonApp = "python2.7"
pairFixApp = os.path.dirname(os.path.abspath(__file__)) + '/../src/adaptertrim.py' 

#trimmedFile = "%s-trimmed.fastq" % (os.path.splitext(testInFileName)[0],)
#lowQualFile = "%s-lowquality.fastq" % (os.path.splitext(testInFileName)[0],)


'''
Char   !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
       |              |          |    |        |                              |                     |
Ascii 33             44         59   64       73                            104                   126
Phred  0.2...........15.........26...31........41 
'''


class TestPairFix(unittest.TestCase):
    '''A super class for test classes that performs testing on pairfix (python)'''
    
    currentResult = None
    errorcount = 0
    
    def setUp(self):
        if DEBUG:
            print("\n\nRunning: %s" % self._testMethodName)
        if os.path.exists(self._testMethodName):
            shutil.rmtree(self._testMethodName)
        os.mkdir(self._testMethodName)
        os.chdir(self._testMethodName)
        
        # store error count for later (hack)
        self.errorcount = len(self.currentResult.errors) + len(self.currentResult.failures)
        
    def run(self, result=None):
        self.currentResult = result # remember result for use in tearDown
        unittest.TestCase.run(self, result) # call superclass run method
        
    def tearDown(self):
        os.chdir('..')
        
        # hack to cleanup only when successful
        errors = len(self.currentResult.errors) + len(self.currentResult.failures)
        if errors == self.errorcount and not DEBUG:
            shutil.rmtree(self._testMethodName)
        self.errorcount = errors

    def assertFileLen(self, filename, length, msg):
        f = open(filename, 'r')
        self.assertEqual(len(f.readlines()), length, msg)

    def assertSeqEquals(self, filename, seq, msg):
        handle = open(filename, "rU")
        seqfound = False
        for record in SeqIO.parse(handle, "fastq"):
            self.assertEqual(str(record.seq), str(seq), msg)
            seqfound = True
            break
        self.assertTrue(seqfound, "%s, No sequence in file" % msg)
        handle.close()

## test classes ##

class TestOverlap(TestPairFix):
    '''checks the Overlaps are reported correctly'''
    
    def test_30b_adaptor(self):
        insert = _sequence[:70]
        adaptor = _adaptor[:30]
        writeSeq1([('test', insert + adaptor, qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert) + adaptor, qual((20,)) * 100)])
        runPairFix()
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert), "Read 2 is not trimmed properly")
        
    def test_1b_adaptor(self):
        insert = _sequence[:99]
        adaptor = _adaptor[:1]
        writeSeq1([('test', insert + adaptor, qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert) + adaptor, qual((20,)) * 100)])
        runPairFix()
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert), "Read 2 is not trimmed properly")

    def test_no_adaptor(self):
        insert = _sequence[:100]
        writeSeq1([('test', insert, qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert), qual((20,)) * 100)])
        runPairFix()
        self.assertSeqEquals(testOutFileName1, insert, "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert), "Read 2 is not trimmed properly")

    def test_101_insert(self):
        insert = _sequence[:101]
        writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert)[:100], qual((20,)) * 100)])
        runPairFix()
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert)[:100], "Read 2 is not trimmed properly")

    def test_150_insert(self):
        insert = _sequence[:150]
        writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert)[:100], qual((20,)) * 100)])
        runPairFix()
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert)[:100], "Read 2 is not trimmed properly")

    def test_199_insert(self):
        insert = _sequence[:199]
        writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert)[:100], qual((20,)) * 100)])
        runPairFix()
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert)[:100], "Read 2 is not trimmed properly")

    def test_200_insert(self):
        insert = _sequence[:101]
        writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert)[:100], qual((20,)) * 100)])
        runPairFix()
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert)[:100], "Read 2 is not trimmed properly")

class TestGZip(TestPairFix):
    '''Test the gzip io functions'''
    
    def test_150_insert_gzip(self):
        insert = _sequence[:150]
        writeSeq1([('test', insert[:100], qual((20,)) * 100)], gz=True)
        writeSeq2([('test', reverse(insert)[:100], qual((20,)) * 100)], gz=True)
        runPairFix()
        rc = runcmd(['gunzip', testOutFileName1GZ])
        self.assertEqual(rc, 0, "Gunzip failed to extract read 1 file")
        rc = runcmd(['gunzip', testOutFileName2GZ])
        self.assertEqual(rc, 0, "Gunzip failed to extract read 2 file")
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert)[:100], "Read 2 is not trimmed properly")
    
    def test_150_insert_mixed_gzip(self):
        insert = _sequence[:150]
        writeSeq1([('test', insert[:100], qual((20,)) * 100)])
        writeSeq2([('test', reverse(insert)[:100], qual((20,)) * 100)], gz=True)
        runPairFix()
        rc = runcmd(['gunzip', testOutFileName2GZ])
        self.assertEqual(rc, 0, "Gunzip failed to extract read 2 file")
        self.assertSeqEquals(testOutFileName1, insert[:100], "Read 1 is not trimmed properly")
        self.assertSeqEquals(testOutFileName2, reverse(insert)[:100], "Read 2 is not trimmed properly")
    
    

## support functions ##
def writeSeq1(seqs, gz=False):
    '''Writes sequences to a file'''
    outFile = open(testInFileName1, 'w')
    for seq in seqs:
        outFile.write('@')
        outFile.write(seq[0])
        outFile.write('\n')
        outFile.write(seq[1])
        outFile.write('\n+\n')
        outFile.write(seq[2])
        outFile.write('\n')
    outFile.close()
    if gz:
        runcmd(['gzip', testInFileName1])
        
def writeSeq2(seqs, gz=False):
    '''Writes sequences to a file'''
    outFile = open(testInFileName2, 'w')
    for seq in seqs:
        outFile.write('@')
        outFile.write(seq[0])
        outFile.write('\n')
        outFile.write(seq[1])
        outFile.write('\n+\n')
        outFile.write(seq[2])
        outFile.write('\n')
    outFile.close()
    if gz:
        runcmd(['gzip', testInFileName2])

def qual(quals):
    '''returns a qualitystring of given (tuple/list) phred scores'''
    res = ""
    for sc in quals:
        res += str(unichr(sc+33))
    return res

greatqual = qual((38,))
goodbase = qual((21,))
avgbase = qual((16,))
badbase = qual((14,))

def runPairFix():
    '''Runs the program'''
    if os.path.exists(testInFileName1GZ):
        infile1 = testInFileName1GZ
    else:
        infile1 = testInFileName1
    if os.path.exists(testInFileName2GZ):
        infile2 = testInFileName2GZ
    else:
        infile2 = testInFileName2
    
    args = [pythonApp, pairFixApp]
    if DEBUG:
        args.append("-d")
    args.append("-1")
    args.append(infile1)
    args.append("-2")
    args.append(infile2)
    return runcmd(args)

def runcmd(cmd):
    '''Runs a command on the commandline.  Expects a list containing the cmd and any arguments'''
    fnull = open(os.devnull, 'w')
    if fnull:
        if DEBUG:
            print "CMD: %s" %(cmd,)
            sc = subprocess.call(cmd)
        else:
            sc = subprocess.call(cmd, stdout=fnull, stderr=subprocess.STDOUT)
        fnull.close()
        return sc
    else:
        raise Exception("Unable to hide output of run")
    return 222

## test runner ##
if __name__ == '__main__':
    unittest.main()
