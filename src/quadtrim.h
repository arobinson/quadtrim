/**
 * Copyright (c) 2012-2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * quadtrim.c: version 2 of the quality-trim program which incorporates adaptor 
 *             trimming
 *
 *  Created on: 01/05/2014
 *      Author: arobinson
 */

#ifndef _QUADTRIM_H
#define _QUADTRIM_H



// lengths of buffers.
#define NAME_BUFFER_SIZE 	1024
#define SEQ_BUFFER_SIZE 	2048000

// A type for adapter list indexes
#define ADAPTERIDXTYPE	unsigned short
// a type for storing an adapter score (i.e. maximum score *43)
#define ADAPTERSCORETYPE unsigned

/// data types for various things ///
// alter these if you increase the above sizes significantly

// integer type used for buffer indexes for sequences
// i.e. must be able to store an int from 0 to SEQ_BUFFER_SIZE + 2
#define SEQIDXTYPE		unsigned
#define SEQIDXTYPENULL	(SEQ_BUFFER_SIZE+1) // must be bigger than largest sequence supported
#define SEQIDXTYPENOCHANGE	(SEQ_BUFFER_SIZE+2) // must be bigger than largest sequence supported

// integer type for buffer indexes of name line
// i.e. must be able to store an int from 0 to NAME_BUFFER_SIZE + 20
#define NAMEIDXTYPE		unsigned short

// character type for buffers
// i.e. store a single character used in sequence file
#define BUFFERTYPE		char

// integer type (unsigned) for sequence counts
// i.e. must be able to store a number as large as maximum number of supported sequences
#define SEQCOUNTTYPE	unsigned

// type used for avg qual sum (i.e. should handle 'max phred quality score' (104) * SEQ_BUFFER_SIZE)
#define QUALSUMTYPE		unsigned

// OS Specific options
#define OS_PATH_SEP		'/'
#define OS_EOL			'\n'
#define OS_EOL_LEN		1

#define MAX_ADAPTER_DISCOVERY_LEN	40

/// Trim statuses ///
#define QUAD_OK			0 // must be 0
#define QUAD_FAIL		128
#define QUAD_FAILBIT	7
#define QUAD_PASS		128
#define QUAD_PASSBIT	7

// keep statuses
#define QUAD_SING		1	// Pair failed
#define QUAD_SINGBIT	0
#define QUAD_ADAPT		2	// Adapter trimming occurred
#define QUAD_ADAPTBIT	1
#define QUAD_QUAL		4	// Quality trimming occurred
#define QUAD_QUALBIT	2

// discard statuses
#define QUAD_CHAST		8	// Chastity failed
#define QUAD_CHASTBIT	3
#define QUAD_AVGQU		16	// Average quality too low
#define QUAD_AVGQUBIT	4
#define QUAD_LEN		32	// Length too short
#define QUAD_LENBIT		5
#define QUAD_N			64	// Too many N's
#define QUAD_NBIT		6

#define QUAD_COUNTERS	8 // the highest bit (+1) that would be set if all status's on
#define QUAD_FLAGS		QUAD_N // the highest flag bit used for statuses


#define TRIM_UNTRIM	0	// quality trim not performed
#define TRIM_CHAST	1	// discarded due to chastity fail
#define TRIM_LEN	2	// discarded due to length (too short)
#define TRIM_AVGQU	3	// discarded due to Average quality (too low)
#define TRIM_N		4	// discarded due to too many N's //TODO: this probably isn't needed
#define TRIM_SING	5	// kept (but pair discarded)
#define TRIM_OK		6	// kept (good quality sequence)
#define TRIM_MAX	TRIM_OK

#define ADAPT_UNADAPTERTRIM	0	// adapter trim not performed
#define ADAPT_TRIMMED		7	// some adapter was removed from sequence
#define ADAPT_OK			8	// no adapter trimming required
#define ADAPT_MAX			ADAPT_OK





/**
 * A sequence record including the trimming details
 */
struct Sequence {
	
	// buffers for the 4 lines of fastq file
	BUFFERTYPE name[NAME_BUFFER_SIZE+20]; // allow room for longest status string (18 + 2)
	BUFFERTYPE seq[SEQ_BUFFER_SIZE+2]; 	// allow for \n and \0
	BUFFERTYPE plus[NAME_BUFFER_SIZE+2];
	BUFFERTYPE qual[SEQ_BUFFER_SIZE+2];
	
	// calculate vars
	SEQIDXTYPE seqlen;

	// Adapter storage
	SEQIDXTYPE adapterstart; // start of adaptor
	BUFFERTYPE adaptersb[3]; // place to store the 2 wiped seq bases
	BUFFERTYPE adapterqb[3]; // place to store the 2 wiped qual bases
	SEQIDXTYPE adapterlen; // length of adapter
	
	// trim positions
	SEQIDXTYPE start;
	SEQIDXTYPE end;

	// stores the overall status (@see QUAD_* defines above for possible values
	unsigned short status;
	
	// stores status (@see TRIM_* defines above for possible values)
	unsigned short trimstatus;
	
	// todo: document or remove
	SEQIDXTYPE nBases;
};

// initialises a sequence structure
#define CLEAN_SEQ(seqx) seqx.name[0] = 0;\
	seqx.seq[0] = 0;\
	seqx.plus[0] = 0;\
	seqx.qual[0] = 0;\
	seqx.seqlen = 0;\
	seqx.adaptersb[0] = 0;\
	seqx.adapterqb[0] = 0;\
	seqx.adaptersb[2] = 0;\
	seqx.adapterqb[2] = 0;\
	seqx.adapterlen = 0;\
	seqx.start = 0;\
	seqx.end = SEQIDXTYPENULL;\
	seqx.status = QUAD_OK;\
	seqx.trimstatus = TRIM_UNTRIM;\
	seqx.nBases = 0;


/**
 * Settings that are passed in from the command line options.  Used to pass 
 * these around the various functions that use them.
 */
struct Options {
	
	// mode selection
	unsigned short modeAdapter;     // perform adapter trimming
	unsigned short modeChastity;    // perform chastity filtering
	unsigned short modeQuality;     // perform quality trimming
	unsigned short modeNBases;	    // perform max N base filtering
	
	// Common options
	char ** filenames;
	short filecount;
	unsigned short taboutput;       // print statistics on stdout in tab-delimited format
	unsigned short gz;		        // 1 = gz used, 0 = no gz
	unsigned short overridegz;      // 0 = match input, 1 = output always gz, 2 = output always plain-text
	const char * outputdir;		    // the directory where output files are written.  NULL = CWD, <SRC> = with source files ...
	//unsigned short pairformat;      // 0 = single-end read, 1 = paired-end single alternating, 2 = paired-end 2 files
	
	// Quality Check options
	unsigned short minQuality;      // threshold to consider a base 'poor quality'
	unsigned short minAvgQuality;   // minimum quality of entire sequence after cleaning
	SEQIDXTYPE minLength;           // minimum length to keep (after cleaning), note: used in adapter check also
	SEQIDXTYPE maxPoorBases;        // maximum bases 'poor quality' bases to accept in a sequence
	unsigned short qualOffset;      // the quality encoding offset used in fastq file (i.e.
	unsigned short trimStart;       // 0 = end trim only, 1 = trim start of sequence as well
	SEQIDXTYPE windowSize;          // 0 = don't perform moving-window averaging, 1+ = the size of window to average over
	unsigned short minWinAvgQual;   // the minimum quality for a window average
	short tailGCheck;				// remove G's from the end of sequence (helps remove NextSeqTM problem sequences)

	// Adapter Check options
    unsigned short fastMode; 		// = 0 (False) # skip multiple alignment (i.e. stop after first alignment)
    unsigned short minScore; 		// = 20  # total score final match must achieve
    short matchScore; 				// = 1   # score for each matching base
    short mismatchScore; 			// = -2  # score for each mis-matching base
    short maxMismatch; 				// = 1   # max number of mismatches within a match to accept
    unsigned short dumpAdapter; 	// = 0 (False) # write adapter sequences found to file
    unsigned short adaptermode; 	// 0 = simple align, 1 = 2pass pullout&match, 2= 2pass pullout&match(stored trimpoint)
    double maxAdapterMismatchRate; 	// 0.1 = 1 in 10 mismatch bases
    BUFFERTYPE ** adapters; 		// array of buffers that store known adapter sequences
    ADAPTERIDXTYPE adaptercount; 	// number of adapters currently stored in 'adapters'
	
    // Chasity filter
	int noChastity;		// 1 = Chastity check off, 0 = Chastity check on
    
	// NBase options
	SEQIDXTYPE maxNBases;	// maximum number of N's allowed in sequence
};


/**
 * A node in the adapter tree.  Used to store counts of sequences that are 
 * trimmed so the adapters (common ones) can be identified.  Forms a single-
 * linked tree.
 */
struct TreeNode {
	
	BUFFERTYPE base;
	SEQCOUNTTYPE count;
	SEQCOUNTTYPE terminals; // adapter strings that end at this node
	unsigned short significant;
	
	// A = 0, C = 1, G = 2, T = 3, N = 4 (but not stored)
	struct TreeNode *next[5];
};

/**
 * A structure to store a list of adapters
 */
struct Adapter {
	BUFFERTYPE seq[MAX_ADAPTER_DISCOVERY_LEN+2];
	struct Adapter *next;
};

/**
 * The trim position for a particular sequence.  Forms a single-linked list
 */
struct AdapterTrimPoint {
	
	SEQIDXTYPE pos;
	
	struct AdapterTrimPoint *next;
};

#endif

// ----- [Forward Defs] -----

int isAdapter(BUFFERTYPE *seq1, BUFFERTYPE *seq2, struct Options * opt);

extern struct TreeNode* adapterTreeHead;
int inline initAdapterTree();
int inline addAdapterToTree(BUFFERTYPE *adapter, unsigned maxdepth);
short unsigned findSignificantBranches(struct TreeNode* node, int depth, unsigned short minAdapterLen, double cutoff);
struct Adapter* produceAdapters(struct TreeNode* node, int depth);
int extractAdapters(struct TreeNode* adapterTreeHead, struct Options *opts);
int inline destroyAdapterTree();

int qualityCheckSequence(struct Sequence * seq, struct Options * opt);

