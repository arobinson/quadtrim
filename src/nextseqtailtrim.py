'''
/**
 * Copyright (c) 2012-2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

Created on 9 Jan 2015

@author: arobinson
'''

'''
@NS500546:18:H2HN5BGXX:1:11106:9796:8132 1:N:0:ATCGGTAG+CATTAACG
ATTTTGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG
+
<AAA<FF.FFFFFFFFFFFFFFFFFAFAFFFFFFFFFFFFFFFFFFFAFAFFFFFFFFFFFFAFFFFFFFFFFFFFFFFAFFFFFAFF.FFFFFFFFFFFFFFFFFFFFFAFFFAFFFFFF

@NS500546:18:H2HN5BGXX:1:11106:1738:7666 1:N:0:ATCGGTAG+CATTAACG
TGATGACCTTGTGACAGGAGTTCTGGCATAACATCGCCTAGATGCGAAAGAGCACACGTCTGAACTCCAGTCACATCGGTAGTATCTCGTAAGCAGTCATCTGCTTGAAAAAAAGAAGGGG
+
AAAAA.FFF7FFFF.)FF.FF7)FFAFAF.AF.F<F<F)FA<7FFAFFFA.AA..)FAFFFF<<FF<))F.<)F)F7<A)7F..F<)<.<7)<.)F<)).)...)FF.A.)F)<)F.FAAF

'''

seqs = [
        "TGATGACCTTGTGACAGGAGTTCTGGCATAACATCGCCTAGATGCGAAAGAGCACACGTCTGAACTCCAGTCACATCGGTAGTATCTCGTAAGCAGTCATCTGATTGAAAAAAAGAAGGGG",
        "TGATGACCTTGTGACAGGAGTTCTGGCATAACATCGCCTAGATGCGAAAGAGCACACGTCTGAACTCCAGTCACATCGGTAGTATCTCGTAAGCAGTCATCTGATTGAAAAAAAAAAAAAA",
        "TGATGACCTTGTGACAGGAGTTCTGGCATAACATCGCCTAGATGCGAAAGAGCACACGTCTGAACTCCAGTCACATCGGTAGTATCTCGTAAGCAGTCATCTGCTTGAAAAAAAGAAGGGG",
        "ATTTTGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG",
        "CTCTGCTGATCTGCTCAAAGCCCTCTGCTACCCCAGAGTCAAGGTCGGCAATGAGTTTGTCACCAAAGGCCAGACTGTAGAACAGGTATACAATGCAGTGGGTGCTCTTGCCAAAGCCATCAAAGATAAGATGTACCTGTGGATGGTTACC",
        "GTGAAAGTGACAGACACATCAAAGACACCTGTGAGCTTAGAATGGTCTAAGCCAGAGTATGACGGAGGAGATCGGAAGAGCGTCGTGTAAGGAAAGACTGAAAAAAAAGGAGGTCGCCAAAACATTAAAAAAAAAAAGAGGGGGGGGGAGG",
        "GTGAAAGTGACAGACACATCAAAGACACCTGTGAGCTTAGAATGGTCTAAGCCAGAGTATGACGGAGGAGATCGGAAGAGCGTCGTGTAAGGAAAGACTGAAAAAAAAGGAGGTCGCCAAAACATTAAAAAAAAAAAGAGGGGGGGGAAGG",
        "GTCGGGAGCACCAGAAGGGCCGACCGACCAGCACCAACCCCATCGCCAGCATCTTTGCCTGGACGCGTGGCCTAGAACACCGGGGCAAGTTGGACGGGAACCAGGACCTCATCAGGTTCGCCCAGACCCTGGAGAAGGTGTGTGTCGAGAC",
        "CGGAGGTCGTGGGTAGAGCGTCATGAAGGCCTCAGGGACGCTTCGAGAGTATAAGGTGGTCGGGCGCTGCCTGCAGACCCCCAAAAGCCACACGCCGCCCCTCTATCGCATGAGAATTTTTGCACCTAGATCGGAACAGCGTCGTGTAGGG",
        "CGGTGGTCGCCGTATCATTAGATCGGAAGAGCGTCGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG",

        ]
# seq = "TGATGACCTTGTGACAGGAGTTCTGGCATAACATCGCCTAGATGCGAAAGAGCACACGTCTGAACTCCAGTCACATCGGTAGTATCTCGTAAGCAGTCATCTGATTGAAAAAAAGAAGGGG"
# seq = "TGATGACCTTGTGACAGGAGTTCTGGCATAACATCGCCTAGATGCGAAAGAGCACACGTCTGAACTCCAGTCACATCGGTAGTATCTCGTAAGCAGTCATCTGCTTGAAAAAAAGAAGGGG"
# seq = "ATTTTGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG"

MAXMISMATCHES=5

## support classes ##
class State(object):
    '''Stores the state'''
    def __init__(self, phaseobj=None, errors=None, passes=None, seq=None):
        if phaseobj is None:
            pass
        elif errors is None:
            self.phase = phaseobj.phase
            self.errors = phaseobj.errors
            self.passes = phaseobj.passes
            self.seq = phaseobj.seq
        else:
            self.phase = phaseobj
            self.errors = errors
            self.passes = passes
            self.seq = seq
        
    phase = 'G'
    errors = 0
    passes = 0
    seq = ""
    trimpoint = 0
# end State()


## Main code ##

# perform trimming on each sequence
for seq in seqs:
    oseq = seq
    seq += "|" #flag end
    
    # find end
    # doing this the hard way so its easy to translate C++ later
    i = 0
    while seq[i] != "|":
        i+=1
    
    # work backwards to find 
    
    active = [State()]
    complete = []
    i -= 1
    while i >= 0:
        newactive = []
        for s in active:
            if s.phase == 'G':
                if seq[i] == 'G':
                    s.passes += 1
                    s.seq = "G" + s.seq
                elif seq[i] == 'A':
                    s2 = State(s)
                    s2.passes += 1
                    s2.seq = "A" + s.seq
                    s2.phase = 'A'
                    newactive.append(s2)
                    s.errors += 1
                    s.seq = "x" + s.seq
                else:
                    s.errors += 1
                    s.seq = "x" + s.seq
                newactive.append(s)
            else: # phase == 'A'
                if seq[i] == 'A':
                    s.passes += 1
                    s.seq = "A" + s.seq
                else:
                    s.errors += 1
                    s.seq = "x" + s.seq
                newactive.append(s)
        # next active state
        
        active = []
        for s in newactive:
            if s.errors <= MAXMISMATCHES:
                active.append(s)
            else:
                complete.append(s)
                
        if len(active) == 0:
            break
        i-=1
    
    
    # select the cut points for each sequence
    complete.extend(active)
    
    # calculate the best cut point
    smallest = len(oseq)
    for s in complete: # for each option
        miscount = 0
        hitcount = 0
        hitstart = -1
        okstart = -1
        for pos in xrange(len(s.seq)): # for each base in option
            if s.seq[pos] == "x":
                miscount += 1
                hitstart = -1
            else:
                hitcount += 1
                if hitstart < 0:
                    hitstart = pos
            
            if (hitcount > miscount or miscount >= s.errors):
                if okstart == -1 and hitstart != -1:
                    okstart = hitstart
            elif hitcount < miscount:
                okstart = -1
        
        if okstart >= 0:
            trimpoint = len(oseq) - len(s.seq) + okstart
            if trimpoint < smallest:
                smallest = trimpoint
    
    # perform the cut (and display)
    trimmed = oseq[:smallest]
    print "\nRAWseq: %s" %oseq
    print "Trim:   %s" %(trimmed + ("-"*(len(oseq)-smallest)))
























