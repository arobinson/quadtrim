'''
/**
 * Copyright (c) 2012-2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
A script that searches for paired-end sequences which have an insert size 
smaller than the read size (i.e. adapter read at end).  It trims off any 
overhang. 

Example: reads beyond the insert fragment will return sequencing adaptor.  
pairfix will attempt to align the fragments and remove any tail overhang.
 ----------->>>>>>>>>>>>>>>>>>>>>>>>>>>AAAAAAAAAAA
 AAAAAAAAAAA<<<<<<<<<<<<<<<<<<<<<<<<<<<-----------
where:
- > and < are seq bases (in forward and reverse read)
- A is adaptor base


Created on 04/07/2013

@author: arobinson
'''

import sys, getopt, traceback, os

from Bio import SeqIO

class PairfixSettings(object):
    '''Storage (and defaults) object'''
    
    debug = False
    fastmode = False    # skip multiple alignment warning (i.e. stop after first alignment)
    
    minscore = 20       # total score final match must achieve
    
    matchscore = 1      # score for each matching base
    mismatchscore = -2  # score for each mis-matching base
#    endmismatchscore = -0.5 # score for an end mis-match base
    
    maxmismatch = 1     # max number of mismatches within a match to accept
#    maxendmismatch = 5  # max number of mismatches at either end to accept

version = '0.3.1'
cmdmsg = '''python adaptortrim.py [-d] [-s <int>] [-m <float>] [-p <float>] [-M <int>] [-f] -1 <read1file> -2 <read2file>
python adaptortrim.py -v
python adaptortrim.py -h'''
helpmsg = '''
%s

Version:    %s

 -h         Print this help msg
 -d         Print debug output
 -v         Print version information
 -f         Use fast mode (i.e. don't check for multiple alignments)
 -1 <file1> Read 1 file (fastq)
 -2 <file2> Read 2 file (fastq)
 -s <int>   The minimum score of a match to accept (default: 20)
 -m <float> The score to add for a matching base (default: 1.0)
 -p <float> The penalty to add for a mismatching base (default: -2.0)
 -M <int>   The maximum number of mismatching bases to accept (default: 1)
''' % (cmdmsg, version)




def main(argv):
    '''Main execution point.  Parses arguments and reads input files sequence-
    by-sequence'''
    settings = PairfixSettings()
    read1filename = None
    read2filename = None
    
    # parse arguments
    try:
        opts, args = getopt.getopt(argv,"hdfv1:2:s:m:p:M:",[])
    except getopt.GetoptError:
        print cmdmsg
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print helpmsg
            sys.exit()
        if opt == '-v':
            print "Version: %s" % version
            sys.exit()
        elif opt in ("-1",):
            read1filename = arg
        elif opt in ("-2",):
            read2filename = arg
        elif opt in ("-d",):
            settings.debug = True
        elif opt in ("-f",):
            settings.fastmode = True
        elif opt in ("-s",):
            settings.minscore = arg
        elif opt in ("-m",):
            settings.matchscore = arg
        elif opt in ("-p",):
            settings.mismatchscore = arg
        elif opt in ("-M",):
            settings.maxmismatch = arg
            
    print "File 1:  %s" % read1filename
    print "File 2:  %s" % read2filename
    
    # open files
    if str(read1filename).endswith(".gz"):
        import gzip
        read1file = gzip.open(read1filename, "r")
        write1file = gzip.open("%s-fix.fq.gz" % os.path.splitext(os.path.splitext(read1filename)[0])[0], "w")
    else:
        read1file = open(read1filename, "rU")
        write1file = open("%s-fix.fq" % os.path.splitext(read1filename)[0], "w")
    if str(read2filename).endswith(".gz"):
        import gzip
        read2file = gzip.open(read2filename, "r")
        write2file = gzip.open("%s-fix.fq.gz" % os.path.splitext(os.path.splitext(read2filename)[0])[0], "w")
    else:
        read2file = open(read2filename, "rU")
        write2file = open("%s-fix.fq" % os.path.splitext(read2filename)[0], "w")
        
    read2parser = SeqIO.parse(read2file, "fastq")
    for read1 in SeqIO.parse(read1file, "fastq"):
        try:
            read2 = read2parser.next()
            
            # process the reads
            pairfix(read1, read2, settings, write1file, write2file)
        except StopIteration:
            
            tbstr = traceback.format_exc();
            print('RequestHandler.controller(): Un-handled Exception:\n%s' % (tbstr,))
            print "Error: file 2 shorter than file 1"
            break
        
    #TODO: perform a check to make sure file 1 isn't shorter than file 2
    
    # close files
    if read1file:
        read1file.close()
    if read2file:
        read2file.close()
    if write1file:
        write1file.close()
    if write2file:
        write2file.close()

# a dictionary to compliment bases
comp = {
      'A': 'T',
      'T': 'A',
      'C': 'G',
      'G': 'C',
      'N': '',
      }

def pairfix(read1, read2, settings, outfile1, outfile2):
    '''Checks a pair of sequences for an overlap'''
    
    debug = settings.debug
    
    read1seq = str(read1.seq)
    read2seq = str(read2.seq)[::-1]
    
    read1len = len(read1seq)
    read2len = len(read2seq)
    
    # loop by every possible overlap size
    # Note: offset = position of base 1 in read2 with respect to read1 indexes 
    matched = False
    for offset in xrange(read1len-1, -read2len, -1):
        
        score = 0
        
        # loop over every overlapping base
        # read1baseidx = base we are currently comparing in read1
        read2baseidx = max(0,-offset)
        mismatches = 0
        for read1baseidx in xrange(max(0,offset), min(read1len, offset+read2len), 1):
            
            read1base = read1seq[read1baseidx]
            read2base = read2seq[read2baseidx]
            
            if read1base == comp[read2base]:
                score += settings.matchscore
            else:
                score += settings.mismatchscore
                mismatches += 1
                if mismatches > settings.maxmismatch:
                    score = settings.mismatchscore * 150 # (some really small number)
                    break
            read2baseidx += 1
        
        # check matches quality scores
        if score >= settings.minscore:
            # only do it once (i.e. don't bother for any other matches of this sequence)
            if not matched:
                matched = True
                
                # trim only when pair overlaps start
                if offset < 0: # i.e. too small insert
                    
                    # trim seq1
                    read1phred = read1.letter_annotations['phred_quality']
                    read1.letter_annotations = {} 
                    read1.seq = read1seq[0:(read2len+offset)]
                    read1.letter_annotations['phred_quality'] = read1phred[0:(read2len+offset)]
                    
                    # trim seq2
                    read2seqreal = read2seq[::-1]# reverse sequence order (back to original)
                    read2phred = read2.letter_annotations['phred_quality']
                    read2.letter_annotations = {} 
                    read2.seq = read2seqreal[0:(read2len+offset)]
                    read2.letter_annotations['phred_quality'] = read2phred[0:(read2len+offset)]
                    
                    print "Trimmed: %s" % read1.id
                
                # print extra debug output
                if debug:
                    if offset < 0:
                        print "Score: %s" % score
                        print "R1:    %s%s" % ("-"*(-offset), read1seq)
                        print "R2:    %s%s" % (read2seq,"-"*(len(read1seq)-offset-len(read2seq)))
                        print "R1 tr: %s" % read1.seq
                        print "R2 tr: %s" % read2.seq
                    else:
                        print "Not trimmed: %s" % read1.id
                        print "Score: %s" % score
                        print "R1: %s%s" % (read1seq,"-"*(offset))
                        print "R2: %s%s" % ("-"*(offset), read2seq)
                    
                # write sequences
                outfile1.write(read1.format('fastq'))
                outfile2.write(read2.format('fastq'))
                
                if settings.fastmode:
                    break
            else:
                print "Warning: multiple matches found for '%s'.  Using first only!" % read1.id
                break
    
    # write any unmatched reads
    if not matched:
        # write sequences
        outfile1.write(read1.format('fastq'))
        outfile2.write(read2.format('fastq'))

# auto execute when called
if __name__ == "__main__":
    main(sys.argv[1:])
