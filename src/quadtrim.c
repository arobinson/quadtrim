/**
 * Copyright (c) 2012-2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * quadtrim.c: version 2 of the quality-trim program which incorporates adaptor 
 *             trimming
 *
 *  Created on: 07/02/2014
 *      Author: arobinson
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "quadtrim.h"

// testing definitions (TODO: comment these before release)
//#define USE_ARGTABLE
//#define USE_ZLIB
//#define USE_ZLIB_OLD
//#define DEBUG_PROFILE // display some profiling output on core parts of the code

#ifdef USE_ZLIB
# define USE_ZLIB_ANY
#else
# ifdef USE_ZLIB_OLD
#  define USE_ZLIB_ANY
# endif
#endif

#ifndef VERSION
#define VERSION "dev"
#endif


#ifdef USE_ARGTABLE
#include <argtable2.h>
#else
#include <unistd.h>
#endif

// the string to append to description for each TRIM_* (i.e. TRIM_MAX must be a valid index of this array)
char* statusStr[] = {""," [Chastity]\n"," [Length]\n", " [Average Quality]\n", " [N Bases]\n","","",""};
// a string treated as an array and use a char (int) as lookup to return the translated Base code (handles lowercase too but converts to upper)
//                 "                                      &'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ      abcdefghijklmnopqrstuvwxyz";
char* translate =  "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNTNGNNNCNNNNNNNNNNNNANNNNNNNNNNNNTNGNNNCNNNNNNNNNNNNANNNNNNNNNNN";
// a string treated as an array to convert to upper (valid bases) and rest to lower n's so they don't match translated bases.
char* filterbase = "nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnAnCnnnGnnnnnnnnnnnnTnnnnnnnnnnnnAnCnnnGnnnnnnnnnnnnTnnnnnnnnnnn";
// a string treated as an array (-48) to identify the position in branch array based in the base 
char* treeoffset = "44444444444444444444444444444444444444444444444444444444444444444041444244444444444434444444444440414442444444444444344444444444";

char* citation = "Bibtex: @article {Robinson2019.12.18.870642,\n	author = {Robinson, Andrew J and Ross, Elizabeth M},\n	title = {QuAdTrim: Overcoming computational bottlenecks in sequence quality control},\n	year = {2019},\n	doi = {10.1101/2019.12.18.870642},\n	publisher = {Cold Spring Harbor Laboratory},\n	URL = {https://www.biorxiv.org/content/early/2019/12/19/2019.12.18.870642},\n	eprint = {https://www.biorxiv.org/content/early/2019/12/19/2019.12.18.870642.full.pdf},\n	journal = {bioRxiv}\n}\n";




#ifdef USE_ZLIB
	#include <zlib.h>

	// type/function wrappers
	#define FILE_PTR gzFile
	#define FILE_PTR_DECL(_name_) FILE_PTR _name_
	#define FILE_PTR_INIT(_arr_, _size_) {int i = 0; for (; i < _size_; i++) {_arr_[i] = NULL;}};
	#define FILE_PTR_INIT1(_file_) _file_ = NULL;
	#define OPENREAD(filehandle, filename, _gz_) filehandle = gzopen(filename, "r");
	#define OPENWRITEGZ(filehandle, filename) filehandle = gzopen(filename, "wb");
	#define OPENWRITE(filehandle, filename) filehandle = gzopen(filename, "wbT");
	#define NOTOPEN(file) file == NULL
	#define ISOPEN(file) file != NULL
	#define EXTENSIONGZ ".fq.gz"
	#define EXTENSION ".fq"
	#define GETS(str, len, file) gzgets(file, str, len)
	#define PUTS(str, file) gzputs(file, str)
	#define SEEKSTART(file) gzseek(file, 0L, SEEK_SET)
	#define CLOSE(file) gzclose(file)
#else
#ifdef USE_ZLIB_OLD
	#include <zlib.h>

	/**
	 * A struct to store the file handles for both types of file when libz 
	 * doesn't support write-through (i.e. libz-1.2.5 and below)
	 */
	struct FileHandleGroup {
		short gz;
		gzFile gzfile;
		FILE *plainfile;
	};

	// type/function wrappers
	#define FILE_PTR struct FileHandleGroup
	#define FILE_PTR_DECL(_name_) FILE_PTR (_name_)
	#define FILE_PTR_INIT(_arr_, _size_) {int i = 0; for (; i < _size_; i++) {_arr_[i].gzfile = NULL;_arr_[i].plainfile = NULL;}};
	#define FILE_PTR_INIT1(_file_) _file_.gzfile = NULL; _file_.plainfile = NULL;
	#define OPENREAD(filehandle, filename, _gz_) filehandle.gz = _gz_;\
		if(filehandle.gz==1)filehandle.gzfile = _OPENREADGZ(filename) else filehandle.plainfile = _OPENREADPL(filename);
	#define _OPENREADGZ(filename) gzopen(filename, "r");
	#define _OPENREADPL(filename) fopen(filename, "r");
	#define OPENWRITE(filehandle, filename) filehandle.gz = 0; filehandle.plainfile = _OPENWRITEPL(filename);
	#define OPENWRITEGZ(filehandle, filename) filehandle.gz = 1; filehandle.gzfile = _OPENWRITEGZ(filename);
	#define _OPENWRITEGZ(filename) gzopen(filename, "wb");
	#define _OPENWRITEPL(filename) fopen(filename, "wb");
	#define NOTOPEN(file) (file.gz==1?file.gzfile==NULL:file.plainfile== NULL)
	#define ISOPEN(file) (file.gz==1?file.gzfile!=NULL:file.plainfile!= NULL)
	#define EXTENSIONGZ ".fq.gz"
	#define EXTENSION ".fq"
	#define GETS(str, len, file) (file.gz==1?_GETSGZ(str, len, file.gzfile):_GETSPL(str, len, file.plainfile))
	#define _GETSGZ(str, len, file) gzgets(file, str, len)
	#define _GETSPL(str, len, file) fgets(str, len, file)
	#define PUTS(str, file) (file.gz==1?_PUTSGZ(str, file.gzfile):_PUTSPL(str, file.plainfile))
	#define _PUTSGZ(str, file) gzputs(file, str)
	#define _PUTSPL(str, file) fputs(str, file)
	#define SEEKSTART(file) (file.gz==1?_SEEKSTARTGZ(file.gzfile):_SEEKSTARTPL(file.plainfile))
	#define _SEEKSTARTGZ(file) gzseek(file, 0L, SEEK_SET)
	#define _SEEKSTARTPL(file) fseek (file, 0L, SEEK_SET)
	#define CLOSE(file) (file.gz==1?_CLOSEGZ(file.gzfile):_CLOSEPL(file.plainfile))
	#define _CLOSEGZ(file) gzclose(file)
	#define _CLOSEPL(file) fclose(file)

#else
	// type/function wrappers
	#define FILE_PTR FILE *
	#define FILE_PTR_DECL(_name_) FILE_PTR _name_
	#define FILE_PTR_INIT(_arr_, _size_) {int i = 0; for (; i < _size_; i++) {_arr_[i] = NULL;}};
	#define FILE_PTR_INIT1(_file_) _file_ = NULL;
	#define OPENREAD(filehandle, filename, _gz_) if (_gz_==0)filehandle = fopen(filename, "r");else filehandle=NULL;
	#define OPENWRITEGZ(filehandle, filename) filehandle=NULL;
	#define OPENWRITE(filehandle, filename) filehandle = fopen(filename, "w");
	#define NOTOPEN(file) file == NULL
	#define ISOPEN(file) file != NULL
	#define EXTENSIONGZ ".fq"
	#define EXTENSION ".fq"
	#define GETS(str, len, file) fgets(str, len, file)
	#define PUTS(str, file) fputs(str, file)
	#define SEEKSTART(file) fseek(file, 0L, SEEK_SET)
	#define CLOSE(file) fclose(file)
#endif
#endif



// Stores head-node of adapter trim list
struct AdapterTrimPoint *adapterTrimListHead;
// Stores tail-node of adapter trim list
struct AdapterTrimPoint *adapterTrimListTail;

/**
 * Initialises trim point list
 */
void inline initTrimPointList () {
	adapterTrimListHead = NULL;
	adapterTrimListTail = NULL;
}

/**
 * Adds a point to (the end of) global adapter trim list
 * 
 * @return short, 1 = Success, 0 = allocation failure
 */
short inline pushTrimPoint(SEQIDXTYPE pos) {
	
	struct AdapterTrimPoint* newPoint = malloc(sizeof (struct AdapterTrimPoint));
	
	if (newPoint == NULL)
		return 0;
	
	newPoint->next = NULL;
	newPoint->pos = pos;
	
	if (adapterTrimListHead == NULL) {
		adapterTrimListHead = newPoint;
		adapterTrimListTail = newPoint;
	}
	else {
		adapterTrimListTail->next = newPoint;
		adapterTrimListTail = newPoint;
	}
	return 1;
}

/**
 * Removes a point from (the front of) the global trim list.
 */
SEQIDXTYPE inline popTrimPoint() {
	if (adapterTrimListHead == NULL) {
		return SEQIDXTYPENULL;
	}
	else {
		SEQIDXTYPE pos = adapterTrimListHead->pos;
		struct AdapterTrimPoint *oldPoint = adapterTrimListHead;
		adapterTrimListHead = adapterTrimListHead->next;
		if (adapterTrimListHead == NULL)
			adapterTrimListTail = NULL;
		
		free(oldPoint);
		
		return pos;
	}
}

struct TreeNode* adapterTreeHead;
SEQCOUNTTYPE adapterBaseCount[MAX_ADAPTER_DISCOVERY_LEN];

/**
 * Initialises adapter Tree
 * 
 * @return int, 1 = Success
 */
int inline initAdapterTree() {
	adapterTreeHead = NULL;
	int i = 0;
	for (; i < MAX_ADAPTER_DISCOVERY_LEN; ++i)
		adapterBaseCount[i] = 0;
	return 1;
}

/**
 * Sets initial values for all fields in an Adapter TreeNode
 */
void inline initAdapterTreeNode(struct TreeNode *node) {
	//INFO: these aren't needed since we use calloc which initialises to 0
//	node->count = 0;
//	node->significant = 0;
//	node->terminals = 0;
//	node->next[0] = NULL;
//	node->next[1] = NULL;
//	node->next[2] = NULL;
//	node->next[3] = NULL;
//	node->next[4] = NULL;
}

/**
 * Adds an adapter seq to the adapter tree.
 * 
 * @return int, 1 = Success, 0 = allocation failure
 */
int inline addAdapterToTree(BUFFERTYPE *adapter, unsigned maxdepth) {
	
	int i = 0;			// range: [0, MAX_ADAPTER_DISCOVERY_LEN)
	short offset = 0;	// used to find which branch to go to next. range: [0, 4]
	BUFFERTYPE base;
	struct TreeNode* currentNode = adapterTreeHead;
	
	// safety
	if (adapter != NULL) {
		
		// allocation of head node here so that we can keep reference to it
		if (currentNode == NULL) {
			currentNode = calloc(1, sizeof(struct TreeNode));
			initAdapterTreeNode(currentNode);
			currentNode->base = '-';
			if (currentNode == NULL)
				return 0;
			adapterTreeHead = currentNode;
		}
		
		if ((base = adapter[0]) != '\0')
			currentNode->count++;
		
		// add adapter, base by base
		while ((base = adapter[i]) != '\0') {
			offset = treeoffset[((short)base)] - 48;
			
			if (offset >= 4)
				break;
			
			// allocate branch if needed
			if (currentNode->next[offset] == NULL) {
				currentNode->next[offset] = calloc(1, sizeof(struct TreeNode));
				initAdapterTreeNode(currentNode->next[offset]);
				currentNode->next[offset]->base = base;
				if (currentNode->next[offset] == NULL)
					return 0;
			}
			
			// move to next segment in branch
			currentNode = currentNode->next[offset];
			currentNode->count++;
			adapterBaseCount[i]++;
			
			i++;
			if (i >= maxdepth)
				break;
		}
		if (base == '\0' && currentNode != NULL)
			currentNode->terminals++;
	}
	return 1;
}

/**
 * find which branches contain a significant amount of adapters (Tree-recursive)
 * 
 * @param depth: int, range: [0, MAX_ADAPTER_DISCOVERY_LEN]
 */
short unsigned findSignificantBranches(struct TreeNode* node, int depth, unsigned short minAdapterLen, double cutoff) {
	unsigned short significant = 0;
	if (node != NULL) {
		if (depth > 0)
			node->significant = ((double)node->count) / adapterBaseCount[depth-1] >= cutoff;
		else
			node->significant = 1;
		if (node->significant && depth <= MAX_ADAPTER_DISCOVERY_LEN) {
			depth++;
			significant |= findSignificantBranches(node->next[0], depth, minAdapterLen, cutoff);
			significant |= findSignificantBranches(node->next[1], depth, minAdapterLen, cutoff);
			significant |= findSignificantBranches(node->next[2], depth, minAdapterLen, cutoff);
			significant |= findSignificantBranches(node->next[3], depth, minAdapterLen, cutoff);
//			significant |= findSignificantBranches(node->next[4], depth, minAdapterLen, cutoff);
			significant |= depth > minAdapterLen;
		}
		node->significant = significant; //only keep the significant flag if one of the children is significant and deep enough
	}
	return significant;
}

/**
 * Creates a string representation of each significant adapter
 * 
 * Performs depth first traversal of tree on only significant branches and 
 * produces the adapter strings on the way back down the tree.
 * 
 * @param depth: int, range: [0, MAX_ADAPTER_DISCOVERY_LEN]
 */
struct Adapter* produceAdapters(struct TreeNode* node, int depth) {
	
	struct Adapter* result = NULL;
	struct Adapter* chresult = NULL;
	struct Adapter* tmp = NULL;
	int i = 0;
	
	if (node == NULL || !node->significant)
		return NULL;
	
	// merge all significant children adapters into 1 list
	for (; i<4; i++) {
		chresult = produceAdapters(node->next[i], depth+1);
		if (chresult != NULL) {
			if (result == NULL)
				result = chresult;
			else {
				tmp = result;
				while (tmp->next != NULL)
					tmp = tmp->next;
				tmp->next = chresult;
			}
		}
	}
	
	// if no children adapters then start a new one
	if (result == NULL) {
		result = calloc(1, sizeof(struct Adapter));
	}
	
	// add my base to all adapters
	if (depth > 0 && depth <= MAX_ADAPTER_DISCOVERY_LEN) {
		tmp = result;
		while (tmp != NULL) {
			tmp->seq[depth-1] = node->base;
			tmp = tmp->next;
		}
	}
	
	return result;
}

/**
 * Add an adapter to the Options structure
 */
void inline appendAdapter(struct Options *opts, BUFFERTYPE* adapter) {

	BUFFERTYPE* adaptercopy = NULL;
	size_t adapterlen = strlen(adapter);
	BUFFERTYPE ** adapters = NULL;
	BUFFERTYPE ** oldadapters = opts->adapters;
	
	// adjust adapter array length
	opts->adaptercount++;
	adapters = (BUFFERTYPE**) calloc(opts->adaptercount, sizeof(BUFFERTYPE*));
	if (oldadapters != NULL) {
		int ai = 0;
		for (; ai < opts->adaptercount-1; ai++)
			adapters[ai] = oldadapters[ai];
		free(oldadapters);
	}
	
	// copy in adapter (at end)
	adaptercopy = calloc(adapterlen+1, sizeof(BUFFERTYPE));
	strcpy(adaptercopy, adapter);
	adapters[opts->adaptercount-1] = adaptercopy;
	
	opts->adapters = adapters;
}

/**
 * Searches through the adapter tree to find commonly trimmed sequences which
 * are likely to be adapters.
 */
int extractAdapters(struct TreeNode* adapterTreeHead, struct Options *opts) {
	
	struct Adapter* adapters = NULL;
	struct Adapter* tmp = NULL;
	
//	printf("Counts: ");
//	int i = 0;
//	for (; i<MAX_ADAPTER_DISCOVERY_LEN; i++)
//		printf("%i, ", adapterBaseCount[i]);
//	printf("\n%p\n", adapterTreeHead);
	
	// recurse the tree to find significant branches (ones that are used for 
	//  atleast 0.1 precent of adapters at that length).
	findSignificantBranches(adapterTreeHead, 0, 8, 0.1); //TODO: make these an option
	adapters = produceAdapters(adapterTreeHead, 0);
	
	if (adapters != NULL) {
		while (adapters != NULL) {
			printf("Adapter: %s\n", adapters->seq);
			appendAdapter(opts, (BUFFERTYPE*)&(adapters->seq));
			
			// move next (dispose last)
			tmp = adapters;
			adapters = adapters->next;
			free(tmp);
		}
	}
	else {
		printf("No adapters found!\n");
		fflush(stdout);
		return 0;
	}

	fflush(stdout);
	return 1;
}

/**
 * Free memory used by a branch of an adapter tree.
 */
int destroyAdapterBranch(struct TreeNode* branch) {
	int result = 1;
	if (branch != NULL) {
		// cleanup children
		result &= destroyAdapterBranch(branch->next[0]);
		result &= destroyAdapterBranch(branch->next[1]);
		result &= destroyAdapterBranch(branch->next[2]);
		result &= destroyAdapterBranch(branch->next[3]);
		result &= destroyAdapterBranch(branch->next[4]);
		
		// cleanup self
		free(branch);
	}
	return result;
}

/**
 * Free memory used by the adapter tree
 */
int inline destroyAdapterTree() {
	return destroyAdapterBranch(adapterTreeHead);
}


/**
 * Reads a FastQ sequence from the given file and stores result in seq.
 */
int readSequence(FILE_PTR pSeqFile, struct Sequence * seq)
{
	if (GETS(seq->name, NAME_BUFFER_SIZE, pSeqFile) != NULL
			&& GETS(seq->seq, SEQ_BUFFER_SIZE, pSeqFile) != NULL
			&& GETS(seq->plus, NAME_BUFFER_SIZE, pSeqFile) != NULL
			&& GETS(seq->qual, SEQ_BUFFER_SIZE, pSeqFile) != NULL)
	{
		if (seq->name[0] != '@' || seq->plus[0] != '+') //  || seq->plus[1] != '\n' #AR: removed check to allow annotation on '+' line
			return -4; // out of sync
		return 0; // success
	}
	if (seq->name[0] == '\n')
		return -2; // extra eol
	return -1; // eof
}


/**
 * A macro version of writeSequence to improve performance
 */
#define WRITE_SEQ(seqx, file) if (ISOPEN(file))\
{\
	if ((seqx.status & QUAD_FAIL) > 0) {\
		size_t namelen = strlen(seqx.name) - 1;\
		seqx.name[namelen++] = ' ';\
		seqx.name[namelen++] = '[';\
		short i;\
		for (i = 1; i <= QUAD_FLAGS; i<<=1, namelen++)\
			if ((seqx.status&i) > 0)\
				seqx.name[namelen] = 'Y';\
			else\
				seqx.name[namelen] = '.';\
		seqx.name[namelen++] = ']';\
		seqx.name[namelen++] = '\n';\
		seqx.name[namelen] = 0;\
		PUTS(seqx.name, file);\
	}\
	else\
		PUTS(seqx.name, file);\
	PUTS(&(seqx.seq[seqx.start]), file);\
	PUTS(seqx.plus, file);\
	PUTS(&(seqx.qual[seqx.start]), file);\
}


/**
 * Extracts the length of the extension (if none found 0).
 */
int extLen(const char *filename, size_t length)
{
	//int length=strlen(filename);
	int i = length - 1;
	int c = 0;
	for (; i >= 0; i--)
	{
		c++;
		if (filename[i] == '.')
		{
			if (!(i == length - 3 && filename[i+1] == 'g' && filename[i+2] == 'z'))
				return c;
		}
	}
	
	return 0;
}


int inline min(int a, int b) {
	return (a<b?a:b);
}
int inline max(int a, int b) {
	return (a<b?b:a);
}


/**
 * Checks the chastity fail flag on a pair of (or single) sequences.  It simply
 * searches the sequence header for a ' ' (space), then for a ':' and checks 
 * the next character is a 'Y' (i.e. fails chastity)
 * 
 * @return int, the number of sequences that failed chastity.
 */
int chastityCheckSequences(struct Sequence *seq1, struct Sequence *seq2, struct Options *opts) {

	char chastitySearchChars[] = {' ',':'};
	NAMEIDXTYPE chastityLen = 2;
	NAMEIDXTYPE chastityPos = 0;
	NAMEIDXTYPE namePos = 0;
	char chastityFailChar = 'Y';
	int returncode = 0;
	
	// check first seq
	while (chastityPos < chastityLen && seq1->name[namePos] != '\0')
	{
		if (seq1->name[namePos] == chastitySearchChars[chastityPos])
			chastityPos++;
		namePos++;
	}
	if (seq1->name[namePos] == chastityFailChar) {
		seq1->status |= QUAD_CHAST | QUAD_FAIL;
		returncode++;
	}
	
	// check second seq (if required)
	if (seq2 != NULL) {
		namePos = 0;
		chastityPos = 0;
		while (chastityPos < chastityLen && seq2->name[namePos] != '\0')
		{
			if (seq2->name[namePos] == chastitySearchChars[chastityPos])
				chastityPos++;
			namePos++;
		}
		if (seq2->name[namePos] == chastityFailChar) {
			seq2->status |= QUAD_CHAST | QUAD_FAIL;
			returncode++;
		}
	}
	
	return returncode;
}


/**
 * Global align sequences using a basic no-indel method.
 */
int inline alignSequences(struct Sequence *seq1, struct Sequence *seq2, struct Options *opts) {

	SEQIDXTYPE seq1len = 0, al = 0, alend = 0, alrealend = 0;
	SEQIDXTYPE fb, rb, fbend;
	SEQIDXTYPE mismatch = 0, minmatchlen = 0;
	ADAPTERSCORETYPE score = 0, bestscore = 0;
#ifdef DEBUG_PROFILE
	unsigned short comp = 0;
#endif
	
	seq1len = seq1->seqlen;
	
	// try all possible global alignments (except ones that will fail because they are too short)
	minmatchlen = opts->minScore / opts->matchScore;
	alrealend = 2 * seq1len - 1;
	alend = alrealend - minmatchlen + 1;
	for (al = minmatchlen; al < alend; ++al) {
		score = 0;
		mismatch = 0;
		
		// check each base for match
		fbend = min(alrealend - al, seq1len);
		for (fb = max(seq1len - al - 1, 0), rb = min(alrealend - al -1, seq1len -1); fb < fbend; ++fb, --rb) {
#ifdef DEBUG_PROFILE
			comp++;
#endif
			// compare base1 to translated base2
			if (filterbase[((short)seq1->seq[fb])&127] == translate[((short)seq2->seq[rb])&127])
				score += opts->matchScore;
			else {
				score += opts->mismatchScore;
				mismatch++;
				if (mismatch > opts->maxMismatch)
					break;
			}
		}
		
		// check for hit
		if (score >= opts->minScore && mismatch <= opts->maxMismatch) {
			
			// only take improved alignments
			if (score > bestscore) {
				bestscore = score;
				if (al >= seq1len) {
					seq1->end = alrealend - al - 1;
					seq2->end = seq1->end;
				}
			}
			
			if (opts->fastMode)
				break;
		}
	}
	
#ifdef DEBUG_PROFILE
	printf("Base comparisons: %i\n", comp);
#endif
	
	return 0;
}


/**
 * Performs alignment of sequences to find adapters for seeding 
 * adapterCheckSequences on a later pass.
 * 
 * @return 1 = Success, 0 = allocation failure
 */
int preprocessAdapterCheck(struct Sequence *seq1, struct Sequence *seq2, struct Options *opts) {

	SEQIDXTYPE seqend = 0;

	// check sequence lengths
	seq1->seqlen = strlen(seq1->seq) - OS_EOL_LEN; // NOTE: seq includes trailing newline char(s)
	seq2->seqlen = strlen(seq2->seq) - OS_EOL_LEN;
	if (seq1->seqlen != seq2->seqlen)
		return 51;
	
	// perform the alignment
	alignSequences(seq1, seq2, opts);
	
	// store adapters
	if (seq1->end > 0) {
		seqend = seq1->end;
		
		// store the position we trimmed
		if (pushTrimPoint(seqend) == 0)
			return 0; // out of mem
		
		// store adapters
		if (addAdapterToTree(&(seq1->seq[seqend + 1]), MAX_ADAPTER_DISCOVERY_LEN) == 0)
			return 0;
		if (addAdapterToTree(&(seq2->seq[seqend + 1]), MAX_ADAPTER_DISCOVERY_LEN) == 0)
			return 0;
	}
	else {
		if (pushTrimPoint(SEQIDXTYPENOCHANGE) == 0)
			return 0; // out of mem
	}
	
	return 1;
}

/**
 * Checks the insert size for a paired end read is sufficiently long enough for
 * the read length to not run off into that adaptor and trims end of sequence 
 * if required.  It checks insert size by attempting a fairly strict global 
 * alignment of the pair and checks for the case where the alignment had 
 * dangling ends (which are removed).
 * 
 * Status:
 * - No alignment: no change
 * - 3' overhang: no change
 * - 5' overhang: remove overhang and add ADAPT flag to both
 * 
 * NOTE: you should perform this check prior quality check because it could 
 * lead to extra bases being removed from the end of one or both of the sequences if you perform start trimming.
 * 
 * @return 51 = unequal sequence len, 2 = trimmed sequences but failed length, 1 = trimmed sequences, 0 = no trim (no align, no 5' overhang)
 */
int adapterCheckSequences(struct Sequence *seq1, struct Sequence *seq2, struct Options *opts) {
	
	SEQIDXTYPE trimpoint = 0;
	int returncode = 0;
	
	// check sequence lengths
	seq1->seqlen = strlen(seq1->seq) - OS_EOL_LEN; // NOTE: seq includes trailing newline char(s)
	seq2->seqlen = strlen(seq2->seq) - OS_EOL_LEN;
	if (seq1->seqlen != seq2->seqlen)
		return 51;
	
	// perform the alignment
	// Info: this prevents aligning when it was done already in pass 1
	if (seq1->end == SEQIDXTYPENULL)
		alignSequences(seq1, seq2, opts);
	
	// do the trimming if needed
	if (seq1->end < SEQ_BUFFER_SIZE) {
		
		// check if adapter is correct
		if (opts->adaptercount > 0) {
			if (!isAdapter(&(seq1->seq[seq1->end+1]), &(seq2->seq[seq2->end+1]), opts))
				return returncode;
		}
		
		// mark trim point
		trimpoint = seq1->end + 1;

		seq1->status |= QUAD_ADAPT;
		seq2->status |= QUAD_ADAPT;
		if ((seq1->end + 1) < opts->minLength) {
			seq1->status |= (QUAD_LEN + QUAD_FAIL);
			seq2->status |= (QUAD_LEN + QUAD_FAIL);
			returncode = 2;
		}
		else
			returncode = 1;
	
		// copy the adapter before overwriting it.
		if (opts->dumpAdapter != 0) {
			seq1->adapterlen = seq1->seqlen - trimpoint;
			seq2->adapterlen = seq1->adapterlen;
			seq1->adapterstart = trimpoint+2;
			seq2->adapterstart = trimpoint+2;
			seq1->adaptersb[0] = seq1->seq[trimpoint];
			seq1->adapterqb[0] = seq1->qual[trimpoint];
			seq1->adaptersb[1] = seq1->seq[trimpoint+1];
			seq1->adapterqb[1] = seq1->qual[trimpoint+1];
			seq2->adaptersb[0] = seq2->seq[trimpoint];
			seq2->adapterqb[0] = seq2->qual[trimpoint];
			seq2->adaptersb[1] = seq2->seq[trimpoint+1];
			seq2->adapterqb[1] = seq2->qual[trimpoint+1];
		}
		
		// perform trim
		seq1->seq[trimpoint] = '\n';
		seq2->seq[trimpoint] = '\n';
		seq1->seq[trimpoint+1] = 0;
		seq2->seq[trimpoint+1] = 0;
		seq1->qual[trimpoint] = '\n';
		seq2->qual[trimpoint] = '\n';
		seq1->qual[trimpoint+1] = 0;
		seq2->qual[trimpoint+1] = 0;
	}
	
	return returncode;
}

/**
 * Checks if a pair of sequences are considered adapter.
 * Compares the sequence against known adapters (found in pass1 or provided)
 * 
 * @param seq1, BUFFERTYPE*, read 1 of sequence
 * @param seq2, BUFFERTYPE*, read 2 (if any) of sequence
 * @param opt, Options*, global configuration
 * @return: int, bitmask +1 for seq1 adapter, +2 for seq2 adapter
 */
int isAdapter(BUFFERTYPE *seq1, BUFFERTYPE *seq2, struct Options * opt)
{
	short adaptercount = opt->adaptercount;
	unsigned short result = 0;
	ADAPTERIDXTYPE aci = 0;
	
	// compare seq1 only
	if (seq2 == NULL) {
		for (; aci < adaptercount; aci++) {
			BUFFERTYPE *adapterseq = opt->adapters[aci];
			BUFFERTYPE adapterbase;
			BUFFERTYPE seq1base;
			SEQIDXTYPE baseidx = 0;
			SEQIDXTYPE seq1mismatch = 0;
			//INFO: 'A' is the first valid base in ASCII index scheme \/
			while ((adapterbase = adapterseq[baseidx]) >= 'A' && (seq1base = seq1[baseidx]) >= 'A') { 
				
				if (adapterbase != seq1base)
					seq1mismatch++;
				
				baseidx++;
			}
			
			if (seq1mismatch == 0 || (baseidx * opt->maxAdapterMismatchRate / seq1mismatch) >= 1) {
				result = 1; // is adapter
				break;
			}
		}
	} else { // compare both sequences
		for (; aci < adaptercount; aci++) {
			BUFFERTYPE *adapterseq = opt->adapters[aci];
			BUFFERTYPE adapterbase;
			BUFFERTYPE seq1base, seq2base;
			SEQIDXTYPE baseidx = 0;
			SEQIDXTYPE seq1mismatch = 0, seq2mismatch = 0;
			//INFO: 'A' is the first valid base in ASCII index scheme \/
			while ((adapterbase = adapterseq[baseidx]) >= 'A' && (seq1base = seq1[baseidx]) >= 'A' 
					&& (seq2base = seq2[baseidx]) >= 'A') {
				
				if (adapterbase != seq1base)
					seq1mismatch++;
				if (adapterbase != seq2base)
					seq2mismatch++;
				
				baseidx++;
			}
			
			if (seq1mismatch == 0 || (baseidx * opt->maxAdapterMismatchRate / seq1mismatch) >= 1)
				result |= 1; // is adapter
			if (seq2mismatch == 0 || (baseidx * opt->maxAdapterMismatchRate / seq2mismatch) >= 1)
				result |= 2; // is adapter
			if (result == 3)
				break;
		}
	}
	
	return result;
}


/**
 * Checks the quality of a section of sequence matches criteria.  If its a 
 * 2nd+ positive hit then stores it only if *better* than last.
 */
int checkHit(struct Sequence * seq, struct Options * opt, SEQIDXTYPE start, SEQIDXTYPE end, QUALSUMTYPE qualitySum, SEQIDXTYPE nBases, SEQIDXTYPE lastNonG)
{
	BUFFERTYPE quality = 0;
	SEQIDXTYPE i = end, gi = end;
	SEQIDXTYPE len = 0;
	BUFFERTYPE base = 0;
	unsigned short qualOffset = opt->qualOffset;
	unsigned short minQuality = opt->minQuality + qualOffset;
	unsigned short trimstatus = TRIM_OK;
	
	/// Calculate trim status ///
	if (i < opt->minLength) // length (before back tracking)
		trimstatus = TRIM_LEN;
	else {

		// find last non-G base (if needed)
		if (opt->tailGCheck) {
			while (seq->qual[gi] >= qualOffset) {
				if (seq->seq[gi] != 'G')
					lastNonG = gi;
				gi++;
			}

			// Back track until last good base or last non-G
			--i;
			quality = seq->qual[i];
			while ((quality < minQuality && i > start) || i > lastNonG)
			{
				qualitySum -= quality;

				--i;
				quality = seq->qual[i];

				if (opt->modeNBases >= 1) {
					base = seq->seq[i];
					if (base == 'N' || base == 'n')
						nBases--;
				}
			}
		}
		else
		{
			// Back track until last good base
			--i;
			quality = seq->qual[i];
			while (quality < minQuality && i > start)
			{
				qualitySum -= quality;

				--i;
				quality = seq->qual[i];

				if (opt->modeNBases >= 1) {
					base = seq->seq[i];
					if (base == 'N' || base == 'n')
						nBases--;
				}
			}
		}
		
		// do rest of checks
		len = (i - start + 1);
		if (len < opt->minLength) // length (after back tracking)
			trimstatus = TRIM_LEN;
		else if (opt->modeNBases >= 1 && nBases > opt->maxNBases) // max N bases included
			trimstatus = TRIM_N;
		else {

			// check for low average quality
			double avgQual = (qualitySum / (double) len) - qualOffset;
			if (avgQual < (double)opt->minAvgQuality)
				trimstatus = TRIM_AVGQU;
		}	
	}
	
	/// Check if this is a better match and record trim ///
	if (trimstatus > seq->trimstatus  // better type of match (@see note with TRIM_* definitions at top)
			|| (trimstatus == seq->trimstatus && (seq->end - seq->start + 1) < len)) // or same and longer
	{
		seq->start = start;
		seq->end = i;
		seq->trimstatus = trimstatus;
	}
	
	return 0;
}


/**
 * Checks (and trims if needed) a Sequence for base and average base quality
 * 
 * Status:
 * - No quality trimming needed: no change
 * - Trimmed: QUAL flag (even if failed other check)
 * - AvgQual low: AVGQU flag + FAIL
 * - N base count high: N flag + FAIL
 * - Length too short: LEN + FAIL flags
 * 
 * @return 1 = Pass (trimmed or left alone), 0 = Discard (len, avg qual, nbasecount)
 */
int qualityCheckSequence(struct Sequence * seq, struct Options * opt)
{
	QUALSUMTYPE qualitySum = 0;
	SEQIDXTYPE poorBases = 0;
	unsigned short qualOffset = opt->qualOffset;
	unsigned short minQuality = opt->minQuality + qualOffset; 
	SEQIDXTYPE i = 0;
	BUFFERTYPE base = 0;
	BUFFERTYPE quality = 0;
	unsigned short searchStatus = 0; // 0 = searching for start, 1 = searching for end
	SEQIDXTYPE startBase = 0;
	SEQIDXTYPE nBases = 0;
	unsigned short minWinQuality = opt->minWinAvgQual + qualOffset;
	QUALSUMTYPE winQualSum = 0; // used to sum the quality of each base for window based avg quality check
	SEQIDXTYPE windowSize = opt->windowSize;
	SEQIDXTYPE windowSizeM1 = opt->windowSize - 1;
    SEQIDXTYPE lastNonG = 0;
	unsigned short baseChecks = opt->maxNBases >= 0 || opt->tailGCheck > 0;
	
	
	if (opt->trimStart)
	{
		quality = seq->qual[i]; // note: quality is the ascii value (i.e. quality 33 = phred 0)
		while (quality >= qualOffset) // note: quality == 0 at end of line
		{
			if (searchStatus == 0) { // finding start
				if (quality >= minQuality) {
					startBase = i;
					searchStatus = 1;
					qualitySum = quality;
				}
			}
			else // finding end
			{
				qualitySum += quality;
				
				// check for N's (if required)
				if (baseChecks) {
					base = seq->seq[i];
					if (base == 'N' || base == 'n')
						nBases++;
					else if (base != 'G')
						lastNonG = i;
				}
				
				if (quality < minQuality)
				{
					poorBases++;
					
					if (poorBases > opt->maxPoorBases)
					{
						checkHit(seq, opt, startBase, i, qualitySum, nBases, lastNonG);
						
						// try again
						searchStatus = 0;
						poorBases = 0;
						qualitySum = 0;
						nBases = 0;
					}
				}
			}
			
			// get next quality
			i++;
			quality = seq->qual[i];
		}
		// check the last hit (that ended rather than entered bad section)
		if (searchStatus == 1)
			checkHit(seq, opt, startBase, i, qualitySum, nBases, lastNonG);
		
		// if its still un-trimmed then it never got a good quality base
		if (seq->trimstatus == TRIM_UNTRIM)
			seq->trimstatus = TRIM_LEN;
	}
	else // if end only trimming
	{
		// check for nth poor base (or end)
		quality = seq->qual[i]; // note: quality is the ascii value (i.e. quality 33 = phred 0)
		while (quality >= qualOffset && poorBases <= opt->maxPoorBases) // note: quality == 0 at end of line
		{
			if (quality < minQuality)
			{
				poorBases++;
			}
			
			qualitySum += quality;

			// check for N's (if required)
			if (baseChecks) {
				base = seq->seq[i];
				if (base == 'N' || base == 'n')
					nBases++;
				else if (base != 'G')
					lastNonG = i;
			}
			
			// check window avg quality
			if (windowSize > 0) {
                winQualSum += quality;
                if (i >= windowSizeM1) {
                    if ((winQualSum / windowSize) < minWinQuality) { //NOTE: avg and min are both including the qual offset.
                        break;
                    }

                    winQualSum -= seq->qual[i-windowSizeM1]; // take off qual at front of window
                }
			}

			// get next quality
			i++;
			quality = seq->qual[i];
		}
		
		checkHit(seq, opt, 0, i, qualitySum, nBases, lastNonG);
	}
	
	// trim the sequence (i.e. put end of string character after last good base)
	if (seq->trimstatus == TRIM_OK) {
		seq->qual[seq->end + 1] = '\n';
		seq->seq[seq->end + 1]  = '\n';
		seq->qual[seq->end + 2] = 0;
		seq->seq[seq->end + 2]  = 0;
		
		// add qual trimmed flag if required
		if (seq->start != 0 || seq->end < (i-1))
			seq->status |= (QUAD_QUAL);
		
		return 1;
	}
	else {
		// update master status
		if (seq->start != 0 || seq->end < (i-1))
			seq->status |= (QUAD_QUAL);
		
		if (seq->trimstatus == TRIM_AVGQU)
			seq->status |= QUAD_AVGQU | QUAD_FAIL;
		else if (seq->trimstatus == TRIM_LEN)
			seq->status |= QUAD_LEN | QUAD_FAIL;
		else if (seq->trimstatus == TRIM_N)
			seq->status |= QUAD_N | QUAD_FAIL;
	}
	
	return 0;
}

/**
 * Counts number of N's in sequence and checks it is not too high.
 * 
 * Will alter sequences status by adding QUAD_N and QUAD_FAIL if too many N's.
 * No change to status if passes.
 * 
 * @return 1 when count(N) <= maxNBases, 0 when count(N) > maxNBases
 */
int nBaseCheckSequence(struct Sequence * seq, struct Options * opts) {
	
	SEQIDXTYPE i = 0;
	BUFFERTYPE base = seq->seq[i];
	SEQIDXTYPE ncount = 0;
	
	while (base > '\n') {
		if (filterbase[(short)base] == 'n') 
			ncount++;
		i++;
		base = seq->seq[i];
	}
	
	if (ncount > opts->maxNBases) {
		seq->status |= (QUAD_N + QUAD_FAIL);
		return 0;
	}
	return 1;
}



/**
 * Opens files and orchestrates trimming of each read
 * 
 * @return int, 
 *   1= no (or too many) files, 
 *   2= out of memory (adapter table or trim site during preprocessing)
 *   3= failed to discover adapter sequence
 *   5= failed opening inputfile
 *   6= failed opening outputfile
 *   7= extra newline char on file
 *   8= fastq out of sync (unexpected char) 
 */
int trimsequences(struct Options *opts) {
	
	
	FILE_PTR_DECL(pSeqFile1); // read1 input file
	FILE_PTR_DECL(pSeqFile2); // read2 input file

	FILE_PTR_DECL(pOutFile1); // read1 output file (kept)
	FILE_PTR_DECL(pOutFile2); // read2 output file (kept)
	FILE_PTR_DECL(pOutFileSing); // singleton output file (kept)
	FILE_PTR_INIT1(pOutFile1)
	FILE_PTR_INIT1(pOutFile2)
	FILE_PTR_INIT1(pOutFileSing)
	
	FILE_PTR_DECL(pOutFileDiscard); // discards output file (reject)
	FILE_PTR_DECL(pOutFileAdapter); // adapter seq pieces output file (reject)
	FILE_PTR_INIT1(pOutFileDiscard)
	FILE_PTR_INIT1(pOutFileAdapter)

	FILE * gzfilecheck = NULL;
	unsigned char gzbuff = '\0';
	struct Sequence seq1;
	struct Sequence seq2;
	SEQCOUNTTYPE counters[QUAD_COUNTERS];
	SEQCOUNTTYPE failcounters[QUAD_COUNTERS];
	short paired = 0;
	short returncode = 0;
	const char* file1;
	const char* file2;
	int outdirlen = 0;
	if (opts->outputdir != NULL)
		outdirlen = strlen(opts->outputdir);
	
	// check if we are doing paired reads or not
	if (opts->filecount > 1)
		paired = 1;
	else if (opts->filecount > 0)
		paired = 0;
	else
		return 4;
	
	file1 = opts->filenames[0];
	file2 = opts->filenames[1];
	
	// fragment filename1
	// /home/arobinson/read1.fq
	//                 ^    ^  ^
	int filelen1 = 0;
	int filepos1 = 0, filepos2 = 0;
	int extpos1 = -1, extpos2 = -1;
	int i;
	for (i = 0; file1[i] != '\0'; i++)
		if (file1[i] == OS_PATH_SEP) {
			filepos1 = i+1;
			extpos1 = -1;
		}
		else if (file1[i] == '.' && !(file1[i+1] == 'g' && file1[i+2] == 'z' && file1[i+3] == '\0')) // skip .gz
			extpos1 = i;
	filelen1 = i;
	if (extpos1 == -1) 
		extpos1 = filelen1; // set pos to eol (i.e. no ext)
	
	// check for gzip'ed file1
	gzfilecheck = fopen(file1, "r");
	if (gzfilecheck != NULL) {
		if (fread(&gzbuff, 1, 1, gzfilecheck) == 1 && gzbuff == 0x1f &&
				fread(&gzbuff, 1, 1, gzfilecheck) == 1 && gzbuff == 0x8b)
			opts->gz = 1;
		fclose(gzfilecheck);
	}
	
	// print summary of options
	if (opts->taboutput == 0)
	{
		printf("\n[[QUADTrim]]\n\n");
		
		printf("[Mode] {-m %i}\n", opts->modeAdapter + opts->modeChastity + opts->modeNBases + opts->modeQuality);
		if (opts->modeChastity)
			printf(" - Chastity filter {-m 4}\n");
		if (opts->modeAdapter)
			printf(" - Adapter trim {-m 1}\n");
		if (opts->modeQuality)
			printf(" - Quality trim {-m 2}\n");
		if (opts->modeNBases)
			printf(" - N-base filter {-m 8}\n");
		
		printf("\n[Common options]\n");
		printf("GZip compression:         I: %s, O: %s {-z 0|1|2}\n", opts->gz?"Yes":"No", (opts->gz||opts->overridegz==1)?"Yes":"No");
		printf("Output directory:         %s\n", opts->outputdir==NULL?".":opts->outputdir);
		printf("Input files:              %s\n", paired==1?"Paired":"Single");
		printf("- %s\n", file1);
		if (paired==1)
			printf("- %s\n", file2);
		
		printf("\n[Build options]\n");
# ifdef USE_ZLIB
		printf("ZLIB (gzip):              Supported\n");
# else
#ifdef USE_ZLIB_OLD
		printf("ZLIB (gzip):              Supported (Older version)\n");
#else
		printf("ZLIB (gzip):              NOT Supported\n");
#endif
#endif
#ifdef USE_ARGTABLE
		printf("Arg parser:               argtable2\n");
#else
		printf("Arg parser:               getopt\n");
#endif
		printf("Version:                  %s\n\n", VERSION);
		
//		if (opts->modeChastity) {
//			printf("[Chastity filter]\n");
//		}
		
		if (opts->modeAdapter) {
			printf("[Adapter trim options]\n");
			printf("Match score:              %i {-c}\n", opts->matchScore);
			printf("Mismatch score:           %i {-w}\n", opts->mismatchScore);
			printf("Min alignment score:      %i {-A}\n", opts->minScore);
			printf("Min Length:               %i {-l}\n", opts->minLength);
			printf("Max mismatched bases:     %i {-M}\n", opts->maxMismatch);
			printf("Min recorded adapter len: %i%s {-D}\n", opts->dumpAdapter, (opts->dumpAdapter==0?" (off)":""));
			printf("Fastmode (first-match):   %s {-f}\n", opts->fastMode?"Yes":"No");
			printf("Adapter discovery mode:   %i {-T}\n", opts->adaptermode);
			printf("Adapter mismatch rate:    %.2f {-F}\n", opts->maxAdapterMismatchRate);
			if (opts->adaptercount > 0) {
				printf("Adapters:                    {-S}\n");
				int ai = 0;
				for (; ai < opts->adaptercount; ai++)
					printf("- %s\n", opts->adapters[ai]);
			}
			printf("\n");
		}
		
		if (opts->modeQuality) {
			printf("[Quality trim options]\n");
			printf("Min Quality:              %i {-q}\n", opts->minQuality);
			printf("Min Avg Quality:          %i {-a}\n", opts->minAvgQuality);
			printf("Min Length:               %i {-l}\n", opts->minLength);
			printf("Max Poor Bases:           %i {-p}\n", opts->maxPoorBases);
			printf("Quality (ASCII) offset:   %i%s {-o}\n", opts->qualOffset, (opts->qualOffset==33?" (FastQ standard)":""));
			printf("Trim start:               %s {-s}\n", opts->trimStart == 1?"Yes":"No");
			printf("Trim tail Gs:             %s {-g}\n\n", opts->tailGCheck == 1?"Yes":"No");
		}
		
		if (opts->modeNBases) {
			printf("[N-base filter options]\n");
			printf("Max N Bases:              %i {-N}\n\n", opts->maxNBases);
		}
	}

	// clear counters
	memset(counters,(SEQCOUNTTYPE)0, sizeof(counters));
	memset(failcounters,(SEQCOUNTTYPE)0, sizeof(failcounters));

	
	if (paired==1) // paired
	{
		//filelen moved higher so output can use it.
		int offset1 = 0;
		int filelen2 = 0;
		int offset2 = 0;
		int readstatus1 = 0;
		int readstatus2 = 0;
		int seqcount = 0;
		int insertRC = 0;
//		SEQIDXTYPE trimpos = 0;
#ifndef USE_ZLIB
		unsigned short gz2 = 0;
#endif
		
		// fragment filename 2
		for (i = 0; file2[i] != '\0'; i++)
			if (file2[i] == OS_PATH_SEP) {
				filepos2 = i+1;
				extpos2 = -1;
			}
			else if (file2[i] == '.' && !(file2[i+1] == 'g' && file2[i+2] == 'z' && file2[i+3] == '\0')) // skip .gz
				extpos2 = i;
		filelen2 = i;
		if (extpos2 == -1) 
			extpos2 = filelen2; // set pos to eol (i.e. no ext)
		
		// check for gzip'ed file
#ifndef USE_ZLIB
		gzfilecheck = fopen(file2, "r");
		if (gzfilecheck != NULL) {
			if (fread(&gzbuff, 1, 1, gzfilecheck) == 1 && gzbuff == 0x1f &&
					fread(&gzbuff, 1, 1, gzfilecheck) == 1 && gzbuff == 0x8b)
				gz2 = 1;
			fclose(gzfilecheck);
		}
#endif
		
		// calculate buffer size
		char filename1[filelen1+outdirlen+19]; // generally much bigger than needed.  worst case is file1 just containing a file (i.e. no dir)
		char filename2[filelen2+outdirlen+19];
		
		// open file
		OPENREAD(pSeqFile1, file1, opts->gz);
		OPENREAD(pSeqFile2, file2, gz2);
		if (NOTOPEN(pSeqFile1) || NOTOPEN(pSeqFile2)) {
			if (NOTOPEN(pSeqFile1))
				printf("Unable to open: %s\n", file1);
			else
				CLOSE(pSeqFile1);
			if (NOTOPEN(pSeqFile2))
				printf("Unable to open: %s\n", file2);
			else
				CLOSE(pSeqFile2);
			return 5;
		} else {

			if (opts->overridegz != 0)
				opts->gz = (opts->overridegz == 1?1:0);
			
			// initialise
			CLEAN_SEQ(seq1);
			CLEAN_SEQ(seq2);
			
			// construct output filenames
			if (opts->outputdir == NULL) { // CWD
				strcpy(filename1, &(file1[filepos1]));
				strcpy(filename2, &(file2[filepos2]));
				offset1 = extpos1-filepos1;
				offset2 = extpos2-filepos2;
			} else if (strcmp(opts->outputdir, "<SRC>") == 0) { // with input files
				strcpy(filename1, file1);
				strcpy(filename2, file2);
				offset1 = extpos1;
				offset2 = extpos2;
			}
			else { // some other specified directory
				strcpy(filename1, opts->outputdir);
				strcpy(filename2, opts->outputdir);
				offset1 = outdirlen;
				offset2 = outdirlen;
				if (filename1[outdirlen-1] != OS_PATH_SEP) {
					filename1[outdirlen] = OS_PATH_SEP;
					filename2[outdirlen] = OS_PATH_SEP;
					offset1 ++;
					offset2 ++;
				}
				strcpy(&(filename1[offset1]), &(file1[filepos1]));
				strcpy(&(filename2[offset2]), &(file2[filepos2]));
				offset1 += extpos1-filepos1;
				offset2 += extpos2-filepos2;
			}

			if (opts->taboutput == 0)
				printf("Output files:\n");
			
			// open output files
			if (opts->gz == 1)
			{
				if (opts->taboutput == 0)
					printf("+ Write mode:             gzip\n");
				
				strcpy(&(filename1[offset1]), "-pass" EXTENSIONGZ);
				if (opts->taboutput == 0)
					printf("- Trimmed 1:              %s\n", filename1);
				OPENWRITEGZ(pOutFile1, filename1);
				strcpy(&(filename2[offset2]), "-pass" EXTENSIONGZ);
				if (opts->taboutput == 0)
					printf("- Trimmed 2:              %s\n", filename2);
				OPENWRITEGZ(pOutFile2, filename2);
				
				// construct filename for paired
				int i = 0;
				for(i = 0; i < offset1; i++)
					if (filename1[i] != filename2[i])
						filename1[i] = 'X';
				
				strcpy(&(filename1[offset1]), "-singleton" EXTENSIONGZ);
				if (opts->taboutput == 0)
					printf("- Singleton:              %s\n", filename1);
				OPENWRITEGZ(pOutFileSing, filename1);
				strcpy(&(filename1[offset1]), "-discard" EXTENSIONGZ);
				if (opts->taboutput == 0)
					printf("- Discard:                %s\n", filename1);
				OPENWRITEGZ(pOutFileDiscard, filename1);
				if (opts->dumpAdapter != 0) {
					strcpy(&(filename1[offset1]), "-adapter" EXTENSIONGZ);
					if (opts->taboutput == 0)
						printf("- Adapters:               %s\n", filename1);
					OPENWRITEGZ(pOutFileAdapter, filename1);
				}
			}
			else
			{
				if (opts->taboutput == 0)
					printf("+ Write mode:             plain\n");
				
				strcpy(&(filename1[offset1]), "-pass" EXTENSION);
				if (opts->taboutput == 0)
					printf("- Trimmed 1:              %s\n", filename1);
				OPENWRITE(pOutFile1, filename1);
				strcpy(&(filename2[offset2]), "-pass" EXTENSION);
				if (opts->taboutput == 0)
					printf("- Trimmed 2:              %s\n", filename2);
				OPENWRITE(pOutFile2, filename2);
				
				// construct filename for paired
				int i = 0;
				for(i = 0; i < offset1; i++)
					if (filename1[i] != filename2[i])
						filename1[i] = 'X';
				
				strcpy(&(filename1[offset1]), "-singleton" EXTENSION);
				if (opts->taboutput == 0)
					printf("- Singleton:              %s\n", filename1);
				OPENWRITE(pOutFileSing, filename1);
				strcpy(&(filename1[offset1]), "-discard" EXTENSION);
				if (opts->taboutput == 0)
					printf("- Discard:                %s\n", filename1);
				OPENWRITE(pOutFileDiscard, filename1);
				if (opts->dumpAdapter != 0) {
					strcpy(&(filename1[offset1]), "-adapter" EXTENSION);
					if (opts->taboutput == 0)
						printf("- Adapter:                %s\n", filename1);
					OPENWRITE(pOutFileAdapter, filename1);
				}
			}

			if (NOTOPEN(pOutFile1)
					|| NOTOPEN(pOutFile2)
					|| NOTOPEN(pOutFileSing)
					|| NOTOPEN(pOutFileDiscard)
					|| (opts->dumpAdapter != 0 && NOTOPEN(pOutFileAdapter)))
			{
				fprintf(stderr, "Failed to open one or more output files for writing!\n");
				if (ISOPEN(pSeqFile1))
					CLOSE(pSeqFile1);
				if (ISOPEN(pSeqFile2))
					CLOSE(pSeqFile2);
				if (ISOPEN(pOutFile1))
					CLOSE(pOutFile1);
				if (ISOPEN(pOutFile2))
					CLOSE(pOutFile2);
				if (ISOPEN(pOutFileSing))
					CLOSE(pOutFileSing);
				if (ISOPEN(pOutFileDiscard))
					CLOSE(pOutFileDiscard);
				if (ISOPEN(pOutFileAdapter))
					CLOSE(pOutFileAdapter);
				return 6;
			}
			
			///// PASS 1: perform first pass if required /////
			if (opts->modeAdapter != 0 && opts->adaptermode >= 1) {
				if (opts->taboutput == 0)
					printf("\nPreprocessing ...");
				fflush(stdout);
				
				// read each sequence (pair)
				readstatus1 = readSequence(pSeqFile1, &seq1);
				readstatus2 = readSequence(pSeqFile2, &seq2);
				while (readstatus1 == 0 && readstatus2 == 0)
				{
					if (preprocessAdapterCheck(&seq1, &seq2, opts) == 0) {

						fprintf(stderr, "Out of memory!\n");
						if (ISOPEN(pSeqFile1))
							CLOSE(pSeqFile1);
						if (ISOPEN(pSeqFile2))
							CLOSE(pSeqFile2);
						if (ISOPEN(pOutFile1))
							CLOSE(pOutFile1);
						if (ISOPEN(pOutFile2))
							CLOSE(pOutFile2);
						if (ISOPEN(pOutFileSing))
							CLOSE(pOutFileSing);
						if (ISOPEN(pOutFileDiscard))
							CLOSE(pOutFileDiscard);
						if (ISOPEN(pOutFileAdapter))
							CLOSE(pOutFileAdapter);
						return 6;
					}

					// cleanup
					CLEAN_SEQ(seq1);
					CLEAN_SEQ(seq2);
					
					// read next sequence
					readstatus1 = readSequence(pSeqFile1, &seq1);
					readstatus2 = readSequence(pSeqFile2, &seq2);
				}

				if (opts->taboutput == 0)
					printf(" Done\n");
				
				// extract adapters
				extractAdapters(adapterTreeHead, opts);
				
				if (opts->adaptermode >= 1 && opts->adaptercount == 0) {
					printf("ERROR: Failed to locate adapter sequence reliably.\n");
					printf("You could try running quadtrim with '-T 0 -D 10'\n");
					printf("and reviewing the adapter dump file.  Alternatively\n");
					printf("provide known adapter sequence with the '-S' option.\n");
					return 3;
				}
				
				// cleanup
				destroyAdapterTree();
				SEEKSTART(pSeqFile1);
				SEEKSTART(pSeqFile2);
			}

			if (opts->taboutput == 0)
				printf("\nRunning ...");
			fflush(stdout);
			
			///// PASS 2: read each sequence (pair) /////
			readstatus1 = readSequence(pSeqFile1, &seq1);
			readstatus2 = readSequence(pSeqFile2, &seq2);
			while (readstatus1 == 0 && readstatus2 == 0)
			{
				// chastity filter
				if (opts->modeChastity != 0) {
					failcounters[QUAD_CHASTBIT] += chastityCheckSequences(&seq1, &seq2, opts);
				}
				
				// adapter check
				if (opts->modeAdapter != 0) {
					if (opts->adaptermode >= 2) {
						// get trim point from pass 1 (adapterCheckSequences will see this and not align a 2nd time)
						seq2.end = seq1.end = popTrimPoint();
					}
					
					if (((seq1.status & QUAD_FAIL) == 0 || (seq2.status & QUAD_FAIL) == 0)) {
						insertRC = adapterCheckSequences(&seq1, &seq2, opts);
						if (insertRC == 51)
							printf("Warning: sequence '%s' not equal sequence length.  Skipping Adapter check!\n", seq1.name);
						else if (insertRC >= 1) {
							if (insertRC == 1)
								counters[QUAD_ADAPTBIT]+=2;
							else
								failcounters[QUAD_LENBIT]+=2;
							
							// write the adapter to file if required
							if (opts->dumpAdapter != 0 && seq1.adapterlen >= opts->dumpAdapter) {
								
								//* // fastq output
									PUTS(seq1.name, pOutFileAdapter);
									PUTS(seq1.adaptersb, pOutFileAdapter);
									PUTS(&(seq1.seq[seq1.adapterstart]), pOutFileAdapter);
									PUTS(seq1.plus, pOutFileAdapter);
									PUTS(seq1.adapterqb, pOutFileAdapter);
									PUTS(&(seq1.qual[seq1.adapterstart]), pOutFileAdapter);
									
									PUTS(seq2.name, pOutFileAdapter);
									PUTS(seq2.adaptersb, pOutFileAdapter);
									PUTS(&(seq2.seq[seq2.adapterstart]), pOutFileAdapter);
									PUTS(seq2.plus, pOutFileAdapter);
									PUTS(seq2.adapterqb, pOutFileAdapter);
									PUTS(&(seq2.qual[seq2.adapterstart]), pOutFileAdapter);
								/*/   //raw seq
									PUTS(seq1.adaptersb, pOutFileAdapter);
									PUTS(&(seq1.seq[seq1.adapterstart]), pOutFileAdapter);
									PUTS(seq2.adaptersb, pOutFileAdapter);
									PUTS(&(seq2.seq[seq2.adapterstart]), pOutFileAdapter);
								//*/
							}
						}
					}
				}
				
				// quality check 
				if (((seq1.status & QUAD_FAIL) == 0 || (seq2.status & QUAD_FAIL) == 0) && opts->modeQuality != 0) {
					if (qualityCheckSequence(&seq1, opts) == 1) {
						if ((seq1.status&QUAD_QUAL) != 0)
							counters[QUAD_QUALBIT]++;
					} else {
						if (seq1.status&QUAD_AVGQU)
							failcounters[QUAD_AVGQUBIT]++;
						if (seq1.status&QUAD_LEN)
							failcounters[QUAD_LENBIT]++;
						if (seq1.status&QUAD_N)
							failcounters[QUAD_NBIT]++;
					}
					if (qualityCheckSequence(&seq2, opts) == 1) {
						if ((seq2.status&QUAD_QUAL) != 0)
							counters[QUAD_QUALBIT]++;
					} else {
						if (seq2.status&QUAD_AVGQU)
							failcounters[QUAD_AVGQUBIT]++;
						if (seq2.status&QUAD_LEN)
							failcounters[QUAD_LENBIT]++;
						if (seq2.status&QUAD_N)
							failcounters[QUAD_NBIT]++;
					}
				}
				else {
					// nbase check
					if ((seq1.status & QUAD_FAIL) == 0 && 
							opts->modeNBases != 0) {
						if (nBaseCheckSequence(&seq1, opts) == 0)
							failcounters[QUAD_NBIT]++;
					}
				}
				
				// alter singleton status if it happened
				if ((seq1.status & QUAD_FAIL) != (seq2.status & QUAD_FAIL)) {
					if ((seq1.status & QUAD_FAIL) == 0) {
						seq1.status |= QUAD_SING;
						counters[QUAD_SINGBIT] ++;
					}
					else if ((seq2.status & QUAD_FAIL) == 0) {
						seq2.status |= QUAD_SING;
						counters[QUAD_SINGBIT] ++;
					}
				}
				
				// write sequences (and count passes)
				if ((seq1.status & QUAD_FAIL) != 0) {
					WRITE_SEQ(seq1, pOutFileDiscard);
					failcounters[QUAD_FAILBIT] ++;
				}
				else if ((seq1.status & QUAD_SING) != 0) {
					WRITE_SEQ(seq1, pOutFileSing);
					counters[QUAD_PASSBIT] ++;
				}
				else {
					WRITE_SEQ(seq1, pOutFile1);
					counters[QUAD_PASSBIT] ++;
				}
				if ((seq2.status & QUAD_FAIL) != 0) {
					WRITE_SEQ(seq2, pOutFileDiscard);
					failcounters[QUAD_FAILBIT] ++;
				}
				else if ((seq2.status & QUAD_SING) != 0) {
					WRITE_SEQ(seq2, pOutFileSing);
					counters[QUAD_PASSBIT] ++;
				}
				else {
					WRITE_SEQ(seq2, pOutFile2);
					counters[QUAD_PASSBIT] ++;
				}
				seqcount++;
				
				// cleanup
				CLEAN_SEQ(seq1);
				CLEAN_SEQ(seq2);
				
				// read next sequence
				readstatus1 = readSequence(pSeqFile1, &seq1);
				readstatus2 = readSequence(pSeqFile2, &seq2);
			}
			
			switch (readstatus1) {
			case -2:
				printf("\nWARNING: extra newline at end of file 1\n");
				returncode = 7;
				break;
			case -4:
				printf("\nERROR: out of sync near line %i file 1\n", seqcount * 4 +1);
				printf("INFO:  could be caused by too long seq(>%i) or seqname(>%i)\n", SEQ_BUFFER_SIZE-2, NAME_BUFFER_SIZE-2);
				returncode = 8;
				break;
			}
			switch (readstatus2) {
			case -2:
				printf("\nWARNING: extra newline at end of file 2\n");
				returncode = 7;
				break;
			case -4:
				printf("\nERROR: out of sync near line %i file 2\n", seqcount * 4 +1);
				printf("INFO:  could be caused by too long seq(>%i) or seqname(>%i)\n", SEQ_BUFFER_SIZE-2, NAME_BUFFER_SIZE-2);
				returncode = 8;
				break;
			}
			
			// close files
			if (ISOPEN(pSeqFile1))
				CLOSE(pSeqFile1);
			if (ISOPEN(pSeqFile2))
				CLOSE(pSeqFile2);
			if (ISOPEN(pOutFile1))
				CLOSE(pOutFile1);
			if (ISOPEN(pOutFile2))
				CLOSE(pOutFile2);
			if (ISOPEN(pOutFileSing))
				CLOSE(pOutFileSing);
			if (ISOPEN(pOutFileDiscard))
				CLOSE(pOutFileDiscard);
			if (ISOPEN(pOutFileAdapter))
				CLOSE(pOutFileAdapter);
		}
	}
	else		// singleton
	{
		int offset = 0;
		char filename[filelen1+19];
		int readstatus1 = 0;
		int seqcount = 0;
		
		// open file
		OPENREAD(pSeqFile1, file1, opts->gz);
		if (NOTOPEN(pSeqFile1)) {
			printf("Unable to open: %s\n", file1);
			return 5;
		} else {
			
			if (opts->overridegz != 0)
				opts->gz = (opts->overridegz == 1?1:0);

			// initialise
			CLEAN_SEQ(seq1);
			
			// construct output filenames
			if (opts->outputdir == NULL) { // CWD
				strcpy(filename, &(file1[filepos1]));
				offset = extpos1-filepos1;
			} else if (strcmp(opts->outputdir, "<SRC>") == 0) { // with input files
				strcpy(filename, file1);
				offset = extpos1;
			}
			else { // some other specified directory
				strcpy(filename, opts->outputdir);
				offset = outdirlen;
				if (filename[outdirlen-1] != OS_PATH_SEP) {
					filename[outdirlen] = OS_PATH_SEP;
					offset ++;
				}
				strcpy(&(filename[offset]), &(file1[filepos1]));
				offset += extpos1-filepos1;
			}
			
			// open output files
			if (opts->taboutput == 0)
				printf("\nOutfiles:\n");
			if (opts->gz == 1)
			{
				strcpy(&(filename[offset]), "-pass" EXTENSIONGZ);
				if (opts->taboutput == 0)
					printf("- Trimmed:                %s\n", filename);
				OPENWRITEGZ(pOutFile1, filename);
				strcpy(&(filename[offset]), "-discard" EXTENSIONGZ);
				if (opts->taboutput == 0)
					printf("- Low Quality:            %s\n", filename);
				OPENWRITEGZ(pOutFileDiscard, filename);
			}
			else
			{
				strcpy(&(filename[offset]), "-pass" EXTENSION);
				if (opts->taboutput == 0)
					printf("- Trimmed:                %s\n", filename);
				OPENWRITE(pOutFile1, filename);
				strcpy(&(filename[offset]), "-discard" EXTENSION);
				if (opts->taboutput == 0)
					printf("- Low Quality:            %s\n", filename);
				OPENWRITE(pOutFileDiscard, filename);
			}
			
			if (NOTOPEN(pOutFile1)
					|| NOTOPEN(pOutFileDiscard)
					)
			{
				printf("Failed to open one or more output files for writing!\n");
				if (ISOPEN(pSeqFile1))
					CLOSE(pSeqFile1);
				if (ISOPEN(pOutFile1))
					CLOSE(pOutFile1);
				if (ISOPEN(pOutFileDiscard))
					CLOSE(pOutFileDiscard);
				return 6;
			}

			if (opts->taboutput == 0)
				printf("\nRunning ...");
			fflush(stdout);
			
			// read each sequence
			while ((readstatus1 = readSequence(pSeqFile1, &seq1)) == 0)
			{
				// Check Chastity
				if (opts->modeChastity >= 1) {
					if (chastityCheckSequences(&seq1, NULL, opts) == 1) {
						counters[QUAD_CHASTBIT]++;
					}
				}
				
				// Check Quality
				if ((seq1.status & QUAD_FAIL) == 0 && opts->modeQuality >= 1) {
					if (qualityCheckSequence(&seq1, opts) == 1) {
						if ((seq1.status&QUAD_QUAL) != 0)
							counters[QUAD_QUALBIT]++;
					} else {
						if (seq1.status&QUAD_AVGQU)
							failcounters[QUAD_AVGQUBIT]++;
						if (seq1.status&QUAD_LEN)
							failcounters[QUAD_LENBIT]++;
						if (seq1.status&QUAD_N)
							failcounters[QUAD_NBIT]++;
					}
				}
				
				// check nbase count
				if ((seq1.status & QUAD_FAIL) == 0 && 
						opts->modeQuality == 0 && // nbase count is done within quality trim so only do it here if not doing quality trim.
						opts->modeNBases > 0) {
					if (nBaseCheckSequence(&seq1, opts) == 0)
						failcounters[QUAD_NBIT]++;
				}
				
				// count status
				if ((seq1.status & QUAD_FAIL) > 0) {// failed
					failcounters[QUAD_FAILBIT]++;
				}
				else {
					counters[QUAD_PASSBIT]++;
				}
				
				// write to file
				if ((seq1.status & QUAD_FAIL) == 0) {
					WRITE_SEQ(seq1, pOutFile1);
				} else {
					WRITE_SEQ(seq1, pOutFileDiscard);
				}
				seqcount++;
				
				// cleanup
				CLEAN_SEQ(seq1);
			}
			
			switch (readstatus1) {
			case -2:
				printf("\nWARNING: extra newline at end of file 1\n");
				returncode = 7;
				break;
			case -4:
				printf("\nERROR: out of sync near line %i file 1\n", seqcount * 4 +1);
				printf("INFO:  could be caused by too long seq(>%i) or seqname(>%i)\n", SEQ_BUFFER_SIZE-2, NAME_BUFFER_SIZE-2);
				returncode = 8;
				break;
			}
			
			// close files
			if (ISOPEN(pSeqFile1))
				CLOSE(pSeqFile1);
			if (ISOPEN(pOutFile1))
				CLOSE(pOutFile1);
			if (ISOPEN(pOutFileDiscard))
				CLOSE(pOutFileDiscard);
		}
	}
	
	if (opts->taboutput == 0)
		printf(" Done\n\n");
	
	// write stats
	SEQCOUNTTYPE total = (counters[QUAD_PASSBIT] + failcounters[QUAD_FAILBIT]);
	double dTotal = total / 100.0;
	
	if (opts->taboutput == 0)
	{
		printf("[Read counts]\n");
		printf("Total:                    %u\n", total);
		printf("Passed:                   %u (%.2f%%)\n", counters[QUAD_PASSBIT], counters[QUAD_PASSBIT] / dTotal);
		if (opts->modeAdapter)
			printf(" - Adapter trimmed:       %u (%.2f%%)\n", counters[QUAD_ADAPTBIT], counters[QUAD_ADAPTBIT] / dTotal);
		if (opts->modeQuality)
			printf(" - Quality trimmed:       %u (%.2f%%)\n", counters[QUAD_QUALBIT], counters[QUAD_QUALBIT] / dTotal);
		if (paired == 1)
			printf(" - Singletons:            %u (%.2f%%)\n", counters[QUAD_SINGBIT], counters[QUAD_SINGBIT] / dTotal);
		
		printf("Discarded:                %u (%.2f%%)\n", failcounters[QUAD_FAILBIT], failcounters[QUAD_FAILBIT] / dTotal);
		if (opts->modeChastity)
			printf(" - Chastity:              %u (%.2f%%)\n", failcounters[QUAD_CHASTBIT], failcounters[QUAD_CHASTBIT] / dTotal);
		if (opts->modeQuality)
			printf(" - Average quality:       %u (%.2f%%)\n", failcounters[QUAD_AVGQUBIT], failcounters[QUAD_AVGQUBIT] / dTotal);
		if (opts->modeAdapter || opts->modeQuality)
			printf(" - Length:                %u (%.2f%%)\n", failcounters[QUAD_LENBIT], failcounters[QUAD_LENBIT] / dTotal);
		if (opts->modeNBases)
			printf(" - N-base count:          %u (%.2f%%)\n", failcounters[QUAD_NBIT], failcounters[QUAD_NBIT] / dTotal);
	}
	else
	{
		printf("Source File1\tSource File2\tTotal\t");
		printf("Passed\tAdapter trimmed\tQuality trimmed\tSingletons\t");
		printf("Discarded\tChastity\tAverage quality\tLength\tN-base count\n");
		
		printf("%s\t%s\t%u\t", file1, paired==1?file2:"-", total);
		printf("%u\t%u\t%u\t%u\t", counters[QUAD_PASSBIT], counters[QUAD_ADAPTBIT], counters[QUAD_QUALBIT], counters[QUAD_SINGBIT]);
		printf("%u\t%u\t%u\t%u\t%u\n", failcounters[QUAD_FAILBIT], failcounters[QUAD_CHASTBIT], failcounters[QUAD_AVGQUBIT], failcounters[QUAD_LENBIT], failcounters[QUAD_NBIT]);
		fflush(stdout);
	}
	return returncode;
}

#ifndef NOMAIN
/**
 * C execution entry point.  Parses/checks arguments and calls that main 
 * trimming function
 * 
 * Return code:
 * 0 = Success (help/citation, normal execution, GZip supported)
 * 1 = Argument error
 * 2 = Insufficient Memory
 * 5 = Failed opening input file, 
 * 6 = Failed opening output file
 * 7 = Extra newline char on file
 * 8 = Fastq out of sync (unexpected char)
 *10 = GZip format not supported 
 */
int main(int argc, char ** argv) {
	
	// common
	int i = 0;
	unsigned short mode = 6;
	int exitcode = 0;

	/// cmdline arg storage ///
	struct Options opts;
	
	/// set defaults ///
	opts.gz = 0;
	// common
	opts.filenames = NULL;
	opts.filecount = 0;
	mode = 6;
	opts.modeAdapter = 0;
	opts.modeQuality = 2;
	opts.modeChastity = 4;
	opts.modeNBases = 0;
	opts.taboutput = 0;
	opts.overridegz = 0;
	opts.outputdir = NULL;
//	opts.pairformat = 0;
	//quality-trim opts
	opts.minQuality = 15;
	opts.minAvgQuality = 20;
	opts.minLength = 50;
	opts.maxPoorBases = 3;
	opts.qualOffset = 33;
	opts.trimStart = 0;
	opts.windowSize = 50;
	opts.minWinAvgQual = 20;
	//adapter-trim opts
	opts.minScore = 20;		    // -A
	opts.matchScore = 1;		// -c
	opts.dumpAdapter = 0;       // -D
	opts.fastMode = 0;          // -f
	opts.maxMismatch = 1; 		// -M
	opts.adaptermode = 2;       // -T
	opts.maxAdapterMismatchRate = 0.1;	// (1 in 10 bases wrong)
	opts.mismatchScore = -2;	// -w
	opts.adapters = NULL;       // -S
	opts.adaptercount = 0;
	//nbases opts
	opts.maxNBases = 1;
	
	
	// arg vars
#ifdef USE_ARGTABLE
	// common options
	struct arg_int  *modeAT 		= arg_int0("m", "mode", 			NULL, 	"Mode: a bit mask selecting required trimming modes. i.e. add numbers for operations you want to perform.  [default: 6]\n                               1 = adapter-trim, \n                               2 = quality-trim, \n                               4 = chastity-filter, \n                               8 = N-base-filter.");
	struct arg_lit  *citationAT		= arg_lit0("C", "citation",					"Print citation information");
	struct arg_lit  *helpAT			= arg_lit0("h", "help", 					"Print this help message");
	struct arg_str	*outdirAT		= arg_str0("O", "output-dir",		NULL,	"Output file directory.                [default: .]\n                               '<SRC>' = with input files");
	struct arg_lit  *tabAT 			= arg_lit0("t", "tab", 						"Display only statistics in tab format");
	struct arg_int  *compressAT	 	= arg_int0("z", "compress", 		NULL, 	"Output compression                    [default:  0]\n                               0 = match input, \n                               1 = gzip, \n                               2 = plain");
	struct arg_lit  *showCompressAT	= arg_lit0("Z", "show-compress",			"Report zlib usage (return code: 0=yes, 10=no)");
	struct arg_file *filesAT		= arg_filen(NULL,NULL,"<file>",1, 2, 		"Specify 1 or 2 input files.  (Two for paired reads)");
	
	// adapter
	struct arg_int  *minAlignScoreAT= arg_int0("A", "min-align-score", 	NULL, 	"Minimum score for an alignment        [default: 20]");
	struct arg_int  *correctScoreAT	= arg_int0("c", "correct-score", 	NULL, 	"Score for a correctly aligned base    [default: 1]");
	struct arg_int  *writeAdaptAT	= arg_int0("D", "write-adapters", 	NULL, 	"Write adapters found to file.         [default: 0 (off)]\n                               0 = off, \n                               1+ = minimum length of adapter to write");
	struct arg_lit  *fastAT			= arg_lit0("f", "fast-adapter",				"Fast mode. Stop at first acceptable match (rather than longest)");
	struct arg_dbl	*adapFilterRate	= arg_dbl0("F", "filter-mismatch-rate", NULL,"Maximum mismatch rate in adapter filtering [default: 0.1]\n                               0.1 = 1 in 10 bases wrong.");
	struct arg_int  *maxMismatchAT	= arg_int0("M", "max-mismatch", 	NULL, 	"Maximum mismatches in alignment       [default: 1]");
	struct arg_str	*adapterAT		= arg_strn("S", "adapter",			NULL, 0, 64, "Provide an adapter sequence to filter with.  Can repeat\n                               more than once to provide more adapters.");
	struct arg_int  *adapTrimModeAT = arg_int0("T", "adapter-trim-mode",NULL, 	"Adapter trim mode                     [default: 2]\n                               0 = 1-pass (no adapter finding), \n                               1 = 2-pass (find adapters then trim adapters), \n                               2 = 2-pass (same as 1 but cache trim points in memory)");
	struct arg_int  *incorrectScoreAT= arg_int0("w", "incorrect-score", NULL, 	"Score for alignment mismatch          [default: -2]");

	// quality
	struct arg_int  *minAvgQualityAT= arg_int0("a", "min-avg-quality", 	NULL, 	"Minimum read average quality          [default: 20]");
	struct arg_lit  *tailGCheckAT	= arg_lit0("g", "tail-g-check",				"Remove G bases from end of sequence");
	struct arg_int  *minLengthAT	= arg_int0("l", "min-length", 		NULL, 	"Minimum read length                   [default: 50]");
	struct arg_int  *offsetAT 		= arg_int0("o", "offset", 			NULL, 	"Phred score + <int> == ASCII code     [default: 33]");
	struct arg_int  *maxPoorBasesAT	= arg_int0("p", "max-poor-bases", 	NULL, 	"Maximum poor quality bases included   [default:  3]");
	struct arg_int  *minQualityAT 	= arg_int0("q", "min-quality", 		NULL, 	"Minimum base cutoff quality           [default: 15]");
	struct arg_lit  *startAT		= arg_lit0("s", "trim-start",				"Trim start of sequences too");
		
	// nbase
	struct arg_int  *maxNBasesAT	= arg_int0("N", "max-n-bases", 		NULL, 	"Maximum N bases included              [default: -1 (Any)]");
	
	// presets
	struct arg_str	*presetsAT		= arg_str0("d", "preset",			NULL,	"Preset defaults.  bulls => -q 20 -a 20 -l 50 -p 3,  v1 => -m 10 -q 20 -a 20 -l 50 -p 3");
	
	// combine options
	struct arg_end  *end = arg_end(20);
	void* argtable[] = {
			modeAT, citationAT, helpAT, tabAT, compressAT, showCompressAT, outdirAT, // common
			minAlignScoreAT, correctScoreAT, writeAdaptAT, fastAT, adapFilterRate, maxMismatchAT, adapterAT, adapTrimModeAT, incorrectScoreAT, // adapter
			minAvgQualityAT, tailGCheckAT, minLengthAT, offsetAT, maxPoorBasesAT, minQualityAT, startAT, // quality
			maxNBasesAT,// nbase
			presetsAT, filesAT, end};
	void* argtablecommon[] = {modeAT, citationAT, helpAT, tabAT, compressAT, showCompressAT, outdirAT, filesAT, end};
	void* argtableadapter[] = {minAlignScoreAT, correctScoreAT, writeAdaptAT, fastAT, adapFilterRate, maxMismatchAT, adapterAT, adapTrimModeAT, incorrectScoreAT, end};
	void* argtablequality[] = {minAvgQualityAT, tailGCheckAT, minLengthAT, offsetAT, maxPoorBasesAT, minQualityAT, startAT, end};
	void* argtablenbase[] = {maxNBasesAT, end};
//	void* argtabledefaults[] = {presetsAT, end};
	
	// other vars
	const char* progname = "quadtrim";
	int nerrors;
	
	// check memory
	if (arg_nullcheck(argtable) != 0) {
		/* NULL entries were detected, some allocations must have failed */
		printf("%s: insufficient memory\n", progname);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 2;
	}
	
	/// add defaults ///
	// common
	modeAT->ival[0] = mode;
	compressAT->ival[0] = opts.overridegz; 
	
	// adapter
	minAlignScoreAT->ival[0] = opts.minScore;
	correctScoreAT->ival[0] = opts.matchScore;
	writeAdaptAT->ival[0] = opts.dumpAdapter;
	adapFilterRate->dval[0] = opts.maxAdapterMismatchRate;
	maxMismatchAT->ival[0] = opts.maxMismatch;
	incorrectScoreAT->ival[0] = opts.mismatchScore;
	adapTrimModeAT->ival[0] = opts.adaptermode;
	
	// quality
	minQualityAT->ival[0] = opts.minQuality;
	minAvgQualityAT->ival[0] = opts.minAvgQuality;
	minLengthAT->ival[0] = opts.minLength;
	maxPoorBasesAT->ival[0] = opts.maxPoorBases;
	offsetAT->ival[0] = opts.qualOffset;
	
	// nbase
	maxNBasesAT->ival[0] = opts.maxNBases;
	
	// parse arguments
	nerrors = arg_parse(argc, argv, argtable);
	
	/// alternative action processing ///
	// help text
	if (helpAT->count > 0) {
		printf("\nUsage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n");
		
		printf("\n Performs various trimming and filtering on single or paired-end reads.\n");
		printf(" For paired-end reads, if a read passes but its pair fails it is separated into\n");
		printf(" a 'singleton' FastQ file.  Output files are given same name as input but with\n");
		printf(" added postfix's for each output:\n");
		printf("  -trimmed   the successful trimmed and filtered reads (x2)\n");
		printf("  -singleton the successful reads whose pair failed*\n");
		printf("  -discard   the sequences which failed*\n");
		printf("  -adapter   the adapter that was trimmed*\n");
		printf("   * filenames changed.  e.g. for read1.fq and read2.fq => readX-discard.fq\n\n\n");
		
		printf("[[Common options]]\n");
		arg_print_glossary(stdout, argtablecommon, "  %-27s %s\n");
		
		printf("\n\n[[Adapter trim options]]\n");
		printf(" Adapter trimming is performed by global aligning paired reads to each other and\n");
		printf(" checking for cases where the alignment over-hangs the start of the reads and\n");
		printf(" trims appropriately\n\n");
		arg_print_glossary(stdout, argtableadapter, "  %-27s %s\n");
		
		printf("\n\n[[Quality trim options]]\n");
		printf(" Trims paired (or single) reads based on the quality score of the reads.  The\n");
		printf(" default mode is to trim only the 3' end but the 5' end can be performed too.\n\n");
		arg_print_glossary(stdout, argtablequality, "  %-27s %s\n");
		
		printf("\n\n[[Chastity filter options]]\n");
		printf(" Removes reads that contain the illuminaTM chastity filter flag\n");

		printf("\n\n[[N base filter options]]\n");
		printf(" Removes reads that contain too many N's (unknown bases) in the sequence\n\n");
		arg_print_glossary(stdout, argtablenbase, "  %-27s %s\n");
		
		printf("\n\n[[Preset defaults]]\n");
		printf("Alternative default settings.  The following options are available:\n\n");
		printf(" -d bulls    => -q 20 -a 20 -l 50 -p 3\n");
		printf(" -d sheep    => -q 20 -a 20 -l 50 -p 3\n");
		printf(" -d v1       => -m 10 -q 20 -a 20 -l 50 -p 3 -O <SRC>\n");

#ifdef USE_ZLIB_ANY
		printf("\n\nZLIB (gzip): Supported\n");
#else
		printf("\n\nZLIB (gzip): NOT Supported\n");
#endif
		printf("Arg parser:  argtable2\n");
		printf("Version:     %s\n\n", VERSION);

		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 0;
	}
	
	// show compression
	if (showCompressAT->count > 0) {
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

#ifdef USE_ZLIB_ANY
		printf("ZLIB (gzip): Supported\n\n");
		return 0;
#else
		printf("ZLIB (gzip): NOT Supported\n\n");
		return 10;
#endif
	}
	
	// show citation
	if (citationAT->count > 0) {
		printf(citation);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 0;
	}
	
	// option errors
	if (nerrors > 0) {
		arg_print_errors(stdout, end, progname);
		printf("Try '%s --help' for more information.\n", progname);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 1;
	}
	
	// brief help
	if (argc == 1) {
		printf("Try '%s --help' for more information.\n", progname);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 1;
	}
	
	/// copy values back after parsing options ///
	// presets
	if (presetsAT->count == 1) {
		if (strcmp(presetsAT->sval[0], "bulls") == 0) {
				opts.minLength = 50;		// -l 50
				opts.minAvgQuality = 20;	// -a 20
				opts.maxPoorBases = 3;	// -p 3
				opts.minQuality = 20;	// -q 20
			} else if (strcmp(presetsAT->sval[0], "sheep") == 0) {
				opts.minLength = 50;		// -l 50
				opts.minAvgQuality = 20;	// -a 20
				opts.maxPoorBases = 3;	// -p 3
				opts.minQuality = 20;	// -q 20
			} else if (strcmp(presetsAT->sval[0], "v1") == 0) {
			opts.modeQuality = 2; //Qual
			opts.modeNBases = 8;  //nbase
			opts.minLength = 50;		// -l 50
			opts.minAvgQuality = 15;	// -a 15
			opts.maxPoorBases = 3;	// -p 3
			opts.minQuality = 20;	// -q 20
			opts.outputdir = "<SRC>";
		} else {
			fprintf (stderr, "Unknown default configuration '%s'.\n", presetsAT->sval[0]);
			return 1;
		}
	}
	
	// common
	mode = modeAT->ival[0];
	opts.modeAdapter = mode & 1;
	opts.modeQuality = mode & 2;
	opts.modeChastity = mode & 4;
	opts.modeNBases = mode & 8;
	if (tabAT->count > 0)
		opts.taboutput = 1;
	opts.overridegz = compressAT->ival[0];
	opts.filenames = (char**)filesAT->filename;//->basename;
	opts.filecount = filesAT->count;
	if (outdirAT->count == 1)
		opts.outputdir = outdirAT->sval[0];
	
	// adapter
	opts.minScore = minAlignScoreAT->ival[0];
	opts.matchScore = correctScoreAT->ival[0];
	opts.dumpAdapter = writeAdaptAT->ival[0];
	if (tailGCheckAT->count > 0)
		opts.tailGCheck = 1;
	opts.maxMismatch = maxMismatchAT->ival[0];
	opts.adaptermode = adapTrimModeAT->ival[0];
	opts.maxAdapterMismatchRate = adapFilterRate->dval[0];
	opts.mismatchScore = incorrectScoreAT->ival[0];
	for (i = 0; i < adapterAT->count; ++i)
		appendAdapter(&opts, (BUFFERTYPE*)adapterAT->sval[i]);
	opts.adaptercount = adapterAT->count;
	
	// quality
	opts.minQuality = minQualityAT->ival[0];
	if (fastAT->count > 0)
		opts.fastMode = 1;
	opts.minAvgQuality = minAvgQualityAT->ival[0];
	opts.minLength = minLengthAT->ival[0];
	opts.maxPoorBases = maxPoorBasesAT->ival[0];
	opts.qualOffset = offsetAT->ival[0];
	if (startAT->count > 0)
		opts.trimStart = 1;
	
	// nbase
	opts.maxNBases = maxNBasesAT->ival[0];
	
	// cleanup
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	
#else  // USE_ARGTABLES (i.e. use getopt instead)
	
	int helpflag = 0;
	int index;
	int c;
	char * files[2];
	short filecount = 0;

	opterr = 0;
	while ((c = getopt (argc, argv, "a:A:c:d:D:F:l:m:M:N:o:O:p:q:S:T:w:z:CfghstZ")) != -1)
	switch (c)
	{
		case 'a':
			opts.minAvgQuality = atoi(optarg);
		break;
		case 'A':
			opts.minScore = atoi(optarg);
		break;
		case 'c':
			opts.matchScore = atoi(optarg);
		break;
		case 'C':
			printf(citation);
			return 0;
		case 'd':
			if (strcmp(optarg, "bulls") == 0) {
				opts.minLength = 50;		// -l 50
				opts.minAvgQuality = 20;	// -a 20
				opts.maxPoorBases = 3;	// -p 3
				opts.minQuality = 20;	// -q 20
			} else if (strcmp(optarg, "sheep") == 0) {
					opts.minLength = 50;		// -l 50
					opts.minAvgQuality = 20;	// -a 20
					opts.maxPoorBases = 3;	// -p 3
					opts.minQuality = 20;	// -q 20
				} else if (strcmp(optarg, "v1") == 0) {
				opts.modeQuality = 2; //Qual
				opts.modeNBases = 8;  //nbase
				opts.minLength = 50;		// -l 50
				opts.minAvgQuality = 15;	// -a 15
				opts.maxPoorBases = 3;	// -p 3
				opts.minQuality = 20;	// -q 20
				opts.outputdir = "<SRC>";
			} else {
				fprintf (stderr, "Unknown default configuration '%s'.\n", optarg);
				return 1;
			}
		break;
		case 'D':
			opts.dumpAdapter = atoi(optarg);
		break;
		case 'F':
			opts.maxAdapterMismatchRate = atof(optarg);
		break;
		case 'l':
			opts.minLength = atoi(optarg);
		break;
		case 'm':
			mode = atoi(optarg);
			opts.modeAdapter = mode & 1;
			opts.modeQuality = mode & 2;
			opts.modeChastity = mode & 4;
			opts.modeNBases = mode & 8;
		break;
		case 'M':
			opts.maxMismatch = atoi(optarg);
		break;
		case 'N':
			opts.maxNBases = atoi(optarg);
		break;
		case 'o':
			opts.qualOffset = atoi(optarg);
		break;
		case 'O':
			opts.outputdir = optarg;
		break;
		case 'p':
			opts.maxPoorBases = atoi(optarg);
		break;
		case 'q':
			opts.minQuality = atoi(optarg);
		break;
		case 'w':
			opts.mismatchScore = atoi(optarg);
		break;
		case 'z':
			opts.overridegz = atoi(optarg);
		break;
		case 'f':
			opts.fastMode = 1;
		break;
		case 'g':
			opts.tailGCheck = 1;
		break;
		case 'h':
			helpflag = 1;
		break;
		case 's':
			opts.trimStart = 1;
		break;
		case 'S':
			appendAdapter(&opts, (BUFFERTYPE*)optarg);
		break;
		case 't':
			opts.taboutput = 1;
		break;
		case 'T':
			opts.adaptermode = atoi(optarg);
		break;
		case 'Z':
#ifdef USE_ZLIB_ANY
		printf("ZLIB (gzip): Supported\n\n");
		return 0;
#else
		printf("ZLIB (gzip): NOT Supported\n\n");
		return 10;
#endif
		case '?':
			if (optopt == 'a' || optopt == 'A' || optopt == 'c' || optopt == 'd' || optopt == 'D' || optopt == 'l' || optopt == 'm'
					|| optopt == 'M'|| optopt == 'N'|| optopt == 'o'|| optopt == 'p'|| optopt == 'q'
					|| optopt == 'w'|| optopt == 'z')
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
			else
				fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			return 1;
		default:
			return 1;
		break;
	}

	// get the file names
	c = 0;
	for (index = optind; index < argc && c <= 2; index++, c++)
	{
		files[c] = argv[index];
		filecount++;
	}
	opts.filenames = files;
	opts.filecount = filecount;
	
	// print help
	if (helpflag == 1)
	{
		//a:A:c:d:D:l:m:M:N:o:O:p:q:T:w:z:CfghstZ
		printf("\n quadtrim [-CfghstZ] [-a <int>] [-A <int>] [-c <int>] [-d <str>] [-D <int>]\n");
		printf("          [-l <int>] [-m <int>] [-M <int>] [-N <int>] [-o <int>] [-p <int>]\n");
		printf("          [-q <int>] [-S <str>] [-T <int>] [-w <int>] [-z [0-2]] <file1> [<file2>]\n\n");
		
		printf(" Performs various trimming and filtering on single or paired-end reads.\n");
		printf(" For paired-end reads, if a read passes but its pair fails it is separated into\n");
		printf(" a 'singleton' FastQ file.  Output files are given same name as input but with\n");
		printf(" added postfix's for each output:\n");
		printf("  -trimmed   the successful trimmed and filtered reads (x2)\n");
		printf("  -singleton the successful reads whose pair failed*\n");
		printf("  -discard   the sequences which failed*\n");
		printf("  -adapter   the adapter that was trimmed*\n");
		printf("   * filenames changed.  e.g. for read1.fq and read2.fq => readX-discard.fq\n\n\n");
		
		printf("[[Common options]]\n");
		printf(" -m <int>    Mode: a bit mask selecting required trimming modes. i.e. add numbers\n");
		printf("             for operations you want to perform.   [default: 6]\n");
		printf("              1 = adapter-trim\n");
		printf("              2 = quality-trim\n");
		printf("              4 = chastity-filter\n");
		printf("              8 = N-base-filter\n");
		printf(" -C          Print citation information\n");
		printf(" -h          Print this help message\n");
		printf(" -O <str>    Output file directory.                [default: .]\n");
		printf("              '<SRC>' = with input files\n");
//		printf(" ");
		printf(" -t          Display only statistics in tab format\n");
		printf(" -z <int>    Output compression                    [default:  0]\n");
		printf("              0 = match input,\n");
		printf("              1 = gzip,\n");
		printf("              2 = plain\n");
		printf(" -Z          Report zlib usage (return code: 0=yes, 10=no)\n");
		printf(" <file1>     Read1 (or single) FastQ file\n");
		printf(" <file2>     Optional Read2 FastQ file\n\n\n");
		
		printf("[[Adapter trim options]]\n");
		printf(" Adapter trimming is performed by global aligning paired reads to each other and\n");
		printf(" checking for cases where the alignment over-hangs the start of the reads and\n");
		printf(" trims appropriately\n\n");
		printf(" -A <int>    Minimum score for an alignment        [default: 20]\n");
		printf(" -c <int>    Score for a correctly aligned base    [default: 1]\n");
		printf(" -D <int>    Write adapters found to file          [default: 0 (off)]\n");
		printf("              0  = off, \n");
		printf("              1+ = minimum length of adapter to write\n");
		printf(" -f          Fast mode. Stop at first acceptable match (rather than longest)\n");
		printf(" -F          Maximum mismatch rate in adapter filtering [default: 0.1]\n");
		printf("              0.1 = 1 in 10 bases wrong\n");
		printf(" -l <int>    Minimum read length                   [default: 50]\n");
		printf(" -M <int>    Maximum mismatches in alignment       [default: 1]\n");
		printf(" -S <str>    Provide an adapter sequence to filter with.  Can repeat\n");
		printf("             more than once to provide more adapters.\n");
		printf(" -T <int>    Adapter discovery mode (pre-process)  [default: 2]\n");
		printf("              0 = off, \n");
		printf("              1 = discovery, \n");
		printf("              2 = discovery with cache.\n");
		printf(" -w <int>    Score for alignment mismatch          [default: -2]\n\n\n");
		
		printf("[[Quality trim options]]\n");
		printf(" Trims paired (or single) reads based on the quality score of the reads.  The\n");
		printf(" default mode is to trim only the 3' end but the 5' end can be performed too.\n\n");
		printf(" -a <int>    Minimum read average quality          [default: 20]\n");
		printf(" -g          Remove G bases from tail of reads\n");
		printf(" -l <int>    Minimum read length                   [default: 50]\n");
		printf(" -o <int>    Phred score + <int> == ASCII code     [default: 33]\n");
		printf(" -p <int>    Maximum poor quality bases included   [default:  3]\n");
		printf(" -q <int>    Minimum base cutoff quality           [default: 15]\n");
		printf(" -s          Trim start of sequences too\n\n\n");
		
		printf("[[Chastity filter options]]\n");
		printf(" Removes reads that contain the illuminaTM chastity filter flag\n\n\n");

		printf("[[N base filter options]]\n");
		printf(" Removes reads that contain too many N's (unknown bases) in the sequence\n\n");
		printf(" -N <int>    Maximum N bases included              [default: 2]\n\n\n");
		
		printf("[[Preset defaults]]\n");
		printf("Alternative default settings.  The following options are available:\n\n");
		printf(" -d bulls    => -q 20 -a 20 -l 50 -p 3\n");
		printf(" -d sheep    => -q 20 -a 20 -l 50 -p 3\n");
		printf(" -d v1       => -m 10 -q 20 -a 20 -l 50 -p 3 -O <SRC>\n");

#ifdef USE_ZLIB_ANY
		printf("\n\n ZLIB (gzip): Supported\n");
#else
		printf("\n\n ZLIB (gzip): NOT Supported\n");
#endif
		printf(" Arg parser:  getopt\n");
		printf(" Version:     %s\n\n", VERSION);
		
		return 0;
	}
	
#endif // end USE_ARGTABLES

	// fix dependent options
	if (opts.minAvgQuality == 0)
		opts.minAvgQuality = opts.minQuality;

	if (opts.filecount <= 0 || opts.filecount > 2) {
		fprintf(stderr, "quadtrim expects either 1 or 2 files only!\n");
		return 1;
	}

	// do the actual processing
	exitcode = trimsequences(&opts);
	
	// cleanup options
	for (i=0; i < opts.adaptercount; i++)
		free(opts.adapters[i]);
	free(opts.adapters);

	return exitcode;
}

#endif // end ifdef NOMAIN

/// EOF ///
